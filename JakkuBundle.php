<?php

namespace Aboutgoods\JakkuBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class JakkuBundle extends Bundle
{
    const VERSION="0.4.0";
}
