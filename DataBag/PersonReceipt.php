<?php
/**
 * Created by PhpStorm.
 * User: aboutgoods
 * Date: 23/11/18
 * Time: 16:24
 */

namespace Aboutgoods\JakkuBundle\DataBag;

use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person\PersonType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Source\ImageType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\TradeAction\ReceiptType;

class PersonReceipt extends AbstractDataBag
{
    public function getBagType()
    {
        return "PERSON_RECEIPT";
    }

    public static function processedDataTypesAvailable()
    {
        return [
            PersonType::class => self::FIELD_OPTIONAL,
            ReceiptType::class => self::FIELD_OPTIONAL
        ];
    }
}