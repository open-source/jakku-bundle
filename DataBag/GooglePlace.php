<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 09/08/18
 * Time: 14:46
 */
namespace Aboutgoods\JakkuBundle\DataBag;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Place\PlaceType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\PointOfSale\OrganizationType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\PointOfSale\PointOfSaleType;

class GooglePlace extends AbstractDataBag
{
    public function getBagType()
    {
        return "GOOGLE_PLACE";
    }

    public static function processedDataTypesAvailable()
    {
        return [
            PlaceType::class        => self::FIELD_OPTIONAL,
            PointOfSaleType::class  => self::FIELD_OPTIONAL,
            OrganizationType::class => self::FIELD_OPTIONAL,
        ];
    }
}