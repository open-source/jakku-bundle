<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 09/08/18
 * Time: 11:45
 */
namespace Aboutgoods\JakkuBundle\DataBag\Data;

use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;
use Aboutgoods\JakkuBundle\DataBag\Source;

class ProcessedData extends Data
{
    protected $source = null;

    /**
     * ProcessedData constructor.
     *
     * @param $source
     */
    public function __construct(AbstractDataType $value,?Source $source = null)
    {
        parent::__construct($value);
        $this->source = $source;
    }

    /**
     * @return mixed
     */
    public function getSource()
    {
        return $this->source;
    }
}