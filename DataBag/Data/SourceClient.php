<?php


namespace Aboutgoods\JakkuBundle\DataBag\Data;


class SourceClient
{
    public $clientIdentifier;
    public $sdkAppId;

    /**
     * @return mixed
     */
    public function getClientIdentifier()
    {
        return $this->clientIdentifier;
    }

    /**
     * @param mixed $clientIdentifier
     *
     * @return SourceClient
     */
    public function setClientIdentifier(?string $clientIdentifier)
    {
        $this->clientIdentifier = $clientIdentifier;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSdkAppId()
    {
        return $this->sdkAppId;
    }

    /**
     * @param mixed $sdkAppId
     *
     * @return SourceClient
     */
    public function setSdkAppId(?string $sdkAppId)
    {
        $this->sdkAppId = $sdkAppId;
        return $this;
    }
}