<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 04/09/2018
 * Time: 10:01
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\RawDataType;

use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractRawDataType;

class ImageRawDataType extends AbstractRawDataType
{
    public function type(): string
    {
        return "IMAGE_URL";
    }
}