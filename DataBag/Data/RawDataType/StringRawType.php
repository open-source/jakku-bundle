<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 09/08/18
 * Time: 14:51
 */
namespace Aboutgoods\JakkuBundle\DataBag\Data\RawDataType;

use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractRawDataType;

class StringRawType extends AbstractRawDataType
{
    public function type(): string
    {
        return "STRING";
    }
}