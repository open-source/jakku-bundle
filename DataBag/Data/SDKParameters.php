<?php



namespace Aboutgoods\JakkuBundle\DataBag\Data;

class SDKParameters
{
    const ITEM_COUNT = "itemCount";
    const NAME = "name";
    const TO = "to";
    const FROM = "from";
    const CUSTOM = "custom";
    const ITEM_ID = "itemId";
    const EAN = "ean";
    const ITEM_NAME = "itemName";
    const PRODUCT = "product";
    const PRODUCT_ID = "productId";
    const ITEM_CATEGORY = "itemCategory";
    const SEARCH_TERM = "searchTerm";
    const COUPON = "coupon";
    const VALUE = "value";
    const TAX = "tax";
    const SHIPPING = "shipping";
    const TRANSACTION_ID = "transactionId";
    const QUANTITY = "quantity";
    const PRICE = "price";
    const UNITY = "unity";
    const START_DATE = "startDate";
    const END_DATE = "endDate";
    const DATE = "date";
    const NONE = "none";
}