<?php

namespace Aboutgoods\JakkuBundle\DataBag\Data;

use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;

abstract class Data
{
    /**
     * @var String
     *
     */
    protected $type;
    /**
     * @var mixed
     */
    protected $value;

    public function getType(): string{
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * ProcessedData constructor.
     *
     * @param $source
     */
    public function __construct(AbstractDataType $value)
    {
        $this->type = $value->type();
        $this->value = $value;
    }
}