<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 03/09/2018
 * Time: 10:39
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Acronym;


class AcronymType
{
    /**
     * @var string|null
     */
    protected $value;

    /**
     * @return null|string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param null|string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

}