<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 03/09/2018
 * Time: 14:22
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Place;


class StatePlaceType extends PlaceType
{
    /**
     * @var CountryPlaceType|null
     */
    protected $country;

    /**
     * @return CountryPlaceType|null
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param CountryPlaceType|null $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    public function type(): string
    {
        return "STATE";
    }

}