<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 03/09/2018
 * Time: 14:56
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Place;


class CountryPlaceType extends PlaceType
{
    /**
     * @see https://fr.wikipedia.org/wiki/ISO_3166-1 or https://www.iso.org/fr/iso-3166-country-codes.html
     * @var string|null
     */
    protected $isoLanguage;

    /**
     * @return null|string
     */
    public function getIsoLanguage()
    {
        return $this->isoLanguage;
    }

    /**
     * @param null|string $isoLanguage
     */
    public function setIsoLanguage($isoLanguage)
    {
        $this->isoLanguage = $isoLanguage;
        return $this;
    }

    public function type(): string
    {
        return "COUNTRY";
    }

}