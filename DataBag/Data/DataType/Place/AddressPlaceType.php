<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 03/09/2018
 * Time: 14:04
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Place;


class AddressPlaceType extends PlaceType
{
    /**
     * @var CityPlaceType|null
     */
    protected $city;
    /**
     * @var string|null
     */
    protected $streetAddress;

    /**
     * @return CityPlaceType|null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param CityPlaceType|null $city
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getStreetAddress()
    {
        return $this->streetAddress;
    }

    /**
     * @param null|string $streetAddress
     */
    public function setStreetAddress($streetAddress)
    {
        $this->streetAddress = $streetAddress;
        return $this;
    }

    public function type(): string
    {
        return "ADDRESS";
    }

}