<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 03/09/2018
 * Time: 14:04
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Place;

class CityPlaceType extends PlaceType
{
    /**
     * @var StatePlaceType|null
     */
    protected $state;
    /**
     * @var string|null
     */
    protected $postalCode;
    /**
     * @var string|null
     */
    protected $population;

    /**
     * @return StatePlaceType|null
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param StatePlaceType|null $state
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param null|string $postalCode
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getPopulation()
    {
        return $this->population;
    }

    /**
     * @param null|string $population
     */
    public function setPopulation($population)
    {
        $this->population = $population;
        return $this;
    }

    public function type(): string
    {
        return "CITY";
    }

}