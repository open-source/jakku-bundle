<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 10/08/18
 * Time: 10:54
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Place;

use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Source\SourceTrait;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Spatial\GeoCoordinate;

class PlaceType extends AbstractDataType
{
    use SourceTrait;

    protected $name;
    protected $uid;
    protected $description;
    /**
     * @var string|null
     */
    protected $idOsm;
    /**
     * @var GeoCoordinate|null
     */
    protected $centerPoint;
    /**
     * @var double[]|null
     */
    protected $boundingBox;
    /**
     * @var int|null
     */
    protected $idGeoName;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getIdOsm()
    {
        return $this->idOsm;
    }

    /**
     * @param null|string $idOsm
     */
    public function setIdOsm($idOsm)
    {
        $this->idOsm = $idOsm;
        return $this;
    }

    /**
     * @return GeoCoordinate|null
     */
    public function getCenterPoint()
    {
        return $this->centerPoint;
    }

    /**
     * @param GeoCoordinate|null $centerPoint
     */
    public function setCenterPoint($centerPoint)
    {
        $this->centerPoint = $centerPoint;
        return $this;
    }

    /**
     * @return double[]|null
     */
    public function getBoundingBox()
    {
        return $this->boundingBox;
    }

    /**
     * @param double[]|null $boundingBox
     */
    public function setBoundingBox($boundingBox)
    {
        $this->boundingBox = $boundingBox;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getIdGeoName()
    {
        return $this->idGeoName;
    }

    /**
     * @param int|null $idGeoName
     */
    public function setIdGeoName($idGeoName)
    {
        $this->idGeoName = $idGeoName;
        return $this;
    }


    public function type(): string
    {
        return "PLACE";
    }
}