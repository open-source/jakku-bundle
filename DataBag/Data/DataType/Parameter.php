<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 14/05/2019
 * Time: 10:07
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType;


class Parameter extends AbstractDataType
{
    protected $key;
    protected $data;

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    public function type(): string
    {
        return 'PARAMETER';
    }
}