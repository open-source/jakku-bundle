<?php


namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType;


class Info extends AbstractDataType
{
    protected $key;
    protected $value;

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     *
     * @return Info
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return array
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param array $value
     *
     * @return Info
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }



    public function type(): string
    {
        return 'INFO';
    }
}