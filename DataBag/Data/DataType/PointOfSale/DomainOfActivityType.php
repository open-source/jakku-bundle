<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 03/09/2018
 * Time: 15:20
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\PointOfSale;


class DomainOfActivityType
{
    /**
     * @var string|null
     */
    protected $uid;
    /**
     * @var string|null
     */
    protected $name;
    /**
     * @var DomainOfActivityType|null
     */
    protected $parentDomainOfActivity;

    /**
     * @return null|string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param null|string $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return DomainOfActivityType|null
     */
    public function getParentDomainOfActivity()
    {
        return $this->parentDomainOfActivity;
    }

    /**
     * @param DomainOfActivityType|null $parentDomainOfActivity
     */
    public function setParentDomainOfActivity($parentDomainOfActivity)
    {
        $this->parentDomainOfActivity = $parentDomainOfActivity;
        return $this;
    }

}