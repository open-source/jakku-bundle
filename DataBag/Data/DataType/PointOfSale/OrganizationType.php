<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 09/08/18
 * Time: 16:34
 */
namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\PointOfSale;

use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Brand\BrandType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Source\SourceTrait;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\TimestampableTrait;

class OrganizationType extends AbstractDataType
{
    use TimestampableTrait;
    use SourceTrait;
    protected $idOrganization;
    protected $name;
    protected $legalName = null;
    protected $legalCode = null;
    protected $phone = null;
    protected $website = null;
    protected $domainOfActivityList = null;
    protected $description = null;
    protected $locale = null;
    protected $logo = null;
    protected $city;
    /**
     * @var OrganizationType|null
     */
    protected $parentOrganization = null;
    /**
     * @var BrandType[]
     */
    protected $brandList = [];

    public function type(): string
    {
        return "ORGANIZATION";
    }

    /**
     * @return mixed
     */
    public function getIdOrganization()
    {
        return $this->idOrganization;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return null
     */
    public function getLegalName()
    {
        return $this->legalName;
    }

    /**
     * @return BrandType[]
     */
    public function getBrandList(): array
    {
        return $this->brandList;
    }

    /**
     * @param BrandType[] $brandList
     */
    public function setBrandList(array $brandList)
    {
        $this->brandList = $brandList;
    }

    /**
     * @return null
     */
    public function getLegalCode()
    {
        return $this->legalCode;
    }

    /**
     * @return null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return null
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @return null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return null
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @return null
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @return mixed
     */
    public function getParentOrganization()
    {
        return $this->parentOrganization;
    }


    /**
     * @param mixed $idOrganization
     */
    public function setIdOrganization($idOrganization)
    {
        $this->idOrganization = $idOrganization;

        return $this;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param null $legalName
     */
    public function setLegalName($legalName)
    {
        $this->legalName = $legalName;

        return $this;
    }

    /**
     * @param null $legalCode
     */
    public function setLegalCode($legalCode)
    {
        $this->legalCode = $legalCode;

        return $this;
    }

    /**
     * @param null $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @param null $website
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * @param null $description
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @param null $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @param null $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * @param mixed $parentOrganization
     */
    public function setParentOrganization($parentOrganization)
    {
        $this->parentOrganization = $parentOrganization;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return null
     */
    public function getDomainOfActivityList()
    {
        return $this->domainOfActivityList;
    }

    /**
     * @param null $domainOfActivityList
     */
    public function setDomainOfActivityList($domainOfActivityList)
    {
        $this->domainOfActivityList = $domainOfActivityList;
    }
}