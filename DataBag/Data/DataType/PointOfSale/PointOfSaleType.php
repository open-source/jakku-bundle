<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 10/08/18
 * Time: 08:42
 */
namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\PointOfSale;

use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Brand\BrandType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Place\AddressPlaceType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Place\CityPlaceType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Source\SourceTrait;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\TimestampableTrait;

class PointOfSaleType extends AbstractDataType
{
    use SourceTrait;
    use TimestampableTrait;

    /**
     * @var string|null
     */
    protected $name;

    /**
     * @var BrandType|null
     */
    protected $brand;
    /**
     * @var string|null
     */
    protected $website;
    /**
     * @var AddressPlaceType|null
     */
    protected $address;

    /**
     * @var string|null
     */
    protected $phone;

    /**
     * @var string|null
     */
    protected $organizationStoreCode;

    /**
     * @var OrganizationType|null
     */
    protected $organization;

    /**
     * @var DomainOfActivityType[]|null
     */
    protected $domainOfActivityList;

    /**
     * @var bool
     */
    protected $isVirtualOnly;

    /**
     * @var CityPlaceType|null
     */
    protected $city;

    /**
     * @return BrandType|null
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param BrandType|null $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param null|string $website
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param null|string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getOrganizationStoreCode()
    {
        return $this->organizationStoreCode;
    }

    /**
     * @param null|string $organizationStoreCode
     */
    public function setOrganizationStoreCode($organizationStoreCode)
    {
        $this->organizationStoreCode = $organizationStoreCode;

        return $this;
    }

    /**
     * @return OrganizationType|null
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param OrganizationType|null $organization
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * @return DomainOfActivityType[]|null
     */
    public function getDomainOfActivityList()
    {
        return $this->domainOfActivityList;
    }

    /**
     * @param DomainOfActivityType[]|null $domainOfActivityList
     */
    public function setDomainOfActivityList($domainOfActivityList)
    {
        $this->domainOfActivityList = $domainOfActivityList;

        return $this;
    }

    /**
     * @return AddressPlaceType|null
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param AddressPlaceType|null $address
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return bool
     */
    public function isVirtualOnly()
    {
        return $this->isVirtualOnly;
    }

    /**
     * @param bool $isVirtualOnly
     */
    public function setIsVirtualOnly($isVirtualOnly)
    {
        $this->isVirtualOnly = $isVirtualOnly;
        return $this;
    }

    /**
     * @return CityPlaceType|null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param CityPlaceType|null $city
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }



    public function type(): string
    {
        return "POINT_OF_SALE";
    }
}