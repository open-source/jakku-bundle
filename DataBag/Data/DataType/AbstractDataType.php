<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 09/08/18
 * Time: 16:53
 */
namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType;

abstract class AbstractDataType
{
    public abstract function type(): string;

}