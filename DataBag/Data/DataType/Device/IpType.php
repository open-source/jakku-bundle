<?php
/**
 * Created by PhpStorm.
 * User: aboutgoods
 * Date: 20/03/19
 * Time: 14:12
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Device;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;

class IpType extends AbstractDataType
{
    protected $mobile;
    protected $wifi;

    /**
     * @return null|String
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param null|String $mobile
     * @return IpType
     */
    public function setMobile(?String $mobile)
    {
        $this->mobile = $mobile;
        return $this;
    }

    /**
     * @return null|String
     */
    public function getWifi()
    {
        return $this->wifi;
    }

    /**
     * @param null|String $wifi
     * @return IpType
     */
    public function setWifi(?String $wifi)
    {
        $this->wifi = $wifi;
        return $this;
    }

    public function type(): string
    {
        return "IP";
    }
}