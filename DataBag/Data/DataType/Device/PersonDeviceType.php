<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 03/09/2018
 * Time: 10:16
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Device;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;

class PersonDeviceType extends AbstractDataType
{
    protected $person;
    protected $device;
    protected $sinceDate;

    /**
     * @return mixed
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @param mixed $person
     * @return PersonDeviceType
     */
    public function setPerson($person)
    {
        $this->person = $person;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * @param mixed $device
     * @return PersonDeviceType
     */
    public function setDevice($device)
    {
        $this->device = $device;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSinceDate()
    {
        return $this->sinceDate;
    }

    /**
     * @param mixed $sinceDate
     * @return PersonDeviceType
     */
    public function setSinceDate($sinceDate)
    {
        $this->sinceDate = $sinceDate;
        return $this;
    }

    public function type(): string
    {
        return "PERSON_DEVICE";
    }
}