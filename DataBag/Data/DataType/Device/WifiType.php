<?php
/**
 * Created by PhpStorm.
 * User: aboutgoods
 * Date: 20/03/19
 * Time: 14:09
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Device;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;

class WifiType extends AbstractDataType
{
    protected $BSSID;
    protected $SSID;

    /**
     * @return null|String
     */
    public function getBSSID()
    {
        return $this->BSSID;
    }

    /**
     * @param null|String $BSSID
     * @return WifiType
     */
    public function setBSSID(?String $BSSID)
    {
        $this->BSSID = $BSSID;
        return $this;
    }

    /**
     * @return null|String
     */
    public function getSSID()
    {
        return $this->SSID;
    }

    /**
     * @param null|String $SSID
     * @return WifiType
     */
    public function setSSID(?String $SSID)
    {
        $this->SSID = $SSID;
        return $this;
    }

    public function type(): string
    {
        return "WIFI";
    }
}