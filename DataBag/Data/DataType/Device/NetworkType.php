<?php


namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Device;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Source\DefaultEntityTrait;

class NetworkType extends AbstractDataType
{
    use DefaultEntityTrait;

    /**
     * @var null|WifiType
     */
    protected $wifi;

    /**
     * @var null|array
     */
    protected $gps;

    /**
     * @var null|IpType
     */
    protected $ip;

    /**
     * @return WifiType|null
     */
    public function getWifi(): ?WifiType
    {
        return $this->wifi;
    }

    /**
     * @param WifiType|null $wifi
     */
    public function setWifi(?WifiType $wifi): self
    {
        $this->wifi = $wifi;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getGps(): ?array
    {
        return $this->gps;
    }

    /**
     * @param array|null $gps
     */
    public function setGps(?array $gps): self
    {
        $this->gps = $gps;
        return $this;
    }

    /**
     * @return IpType|null
     */
    public function getIp(): ?IpType
    {
        return $this->ip;
    }

    /**
     * @param IpType|null $ip
     */
    public function setIp(?IpType $ip): self
    {
        $this->ip = $ip;
        return $this;
    }



    public function type(): string
    {
        return 'NETWORK';
    }
}