<?php
/**
 * Created by PhpStorm.
 * User: aboutgoods
 * Date: 20/03/19
 * Time: 14:19
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Device;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person\ApplicationType;

class DeviceApplicationType extends AbstractDataType
{
    /**
     * @var DeviceType|null
     */
    protected $device;

    /**
     * @var ApplicationType|null
     */
    protected $application;

    /**
     * @var String|null
     */
    protected $instanceId;


    /**
     * @var null|int
     */
    protected $timesLaunched;


    /**
     * @var String|null
     */
    protected $tokenPush;

    /**
     * @return DeviceType|null
     */
    public function getDevice(): ?DeviceType
    {
        return $this->device;
    }

    /**
     * @param DeviceType|null $device
     */
    public function setDevice(?DeviceType $device): self
    {
        $this->device = $device;
        return $this;
    }

    /**
     * @return ApplicationType|null
     */
    public function getApplication(): ?ApplicationType
    {
        return $this->application;
    }

    /**
     * @param ApplicationType|null $application
     */
    public function setApplication(?ApplicationType $application): self
    {
        $this->application = $application;
        return $this;
    }

    /**
     * @return String|null
     */
    public function getInstanceId(): ?String
    {
        return $this->instanceId;
    }

    /**
     * @param String|null $instanceId
     */
    public function setInstanceId(?String $instanceId): self
    {
        $this->instanceId = $instanceId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getTimesLaunched(): ?int
    {
        return $this->timesLaunched;
    }

    /**
     * @param int|null $timesLaunched
     */
    public function setTimesLaunched(?int $timesLaunched): self
    {
        $this->timesLaunched = $timesLaunched;
        return $this;
    }

    /**
     * @return String|null
     */
    public function getTokenPush(): ?String
    {
        return $this->tokenPush;
    }

    /**
     * @param String|null $tokenPush
     */
    public function setTokenPush(?String $tokenPush): self
    {
        $this->tokenPush = $tokenPush;
        return $this;
    }




    public function type(): string
    {
        return "DEVICE_APPLICATION";
    }
}