<?php

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Device;

use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Source\DefaultEntityTrait;

class DeviceType extends AbstractDataType {
    use DefaultEntityTrait;

    /**
     * @var string|null
     */
    protected $osIntVersion;

    /**
     * @var string|null
     */
    protected $batteryLevel;

    /**
     * @var string|null
     */
    protected $macAddress;
    /**
     * @var string|null
     */
    protected $udid;

    /**
     * @var string|null
     */
    protected $model;

    /**
     * @var string|null
     */
    protected $manufacturer;

    /**
     * @var string|null
     */
    protected $os;

    /**
     * @var string|null
     */
    protected $osVersion;

    /**
     * @var string|null
     */
    protected $codeName;

    /**
     * @var string|null
     */
    protected $camera;

    /**
     * @var string|null
     */
    protected $locale;

    /**
     * @var string|null
     */
    protected $currencyCode;

    /**
     * @var array|null
     */
    protected $installedApplicationList;



    public function type(): string {
        return "DEVICE";
    }

    /**
     * @return string|null
     */
    public function getOsIntVersion(): ?string
    {
        return $this->osIntVersion;
    }

    /**
     * @param string|null $osIntVersion
     */
    public function setOsIntVersion(?string $osIntVersion): self
    {
        $this->osIntVersion = $osIntVersion;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBatteryLevel(): ?string
    {
        return $this->batteryLevel;
    }

    /**
     * @param string|null $batteryLevel
     */
    public function setBatteryLevel(?string $batteryLevel): self
    {
        $this->batteryLevel = $batteryLevel;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMacAddress(): ?string
    {
        return $this->macAddress;
    }

    /**
     * @param string|null $macAddress
     */
    public function setMacAddress(?string $macAddress): self
    {
        $this->macAddress = $macAddress;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUdid(): ?string
    {
        return $this->udid;
    }

    /**
     * @param string|null $udid
     */
    public function setUdid(?string $udid): self
    {
        $this->udid = $udid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModel(): ?string
    {
        return $this->model;
    }

    /**
     * @param string|null $model
     */
    public function setModel(?string $model): self
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getManufacturer(): ?string
    {
        return $this->manufacturer;
    }

    /**
     * @param string|null $manufacturer
     */
    public function setManufacturer(?string $manufacturer): self
    {
        $this->manufacturer = $manufacturer;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOs(): ?string
    {
        return $this->os;
    }

    /**
     * @param string|null $os
     */
    public function setOs(?string $os): self
    {
        $this->os = $os;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOsVersion(): ?string
    {
        return $this->osVersion;
    }

    /**
     * @param string|null $osVersion
     */
    public function setOsVersion(?string $osVersion): self
    {
        $this->osVersion = $osVersion;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCodeName(): ?string
    {
        return $this->codeName;
    }

    /**
     * @param string|null $codeName
     */
    public function setCodeName(?string $codeName): self
    {
        $this->codeName = $codeName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCamera(): ?string
    {
        return $this->camera;
    }

    /**
     * @param string|null $camera
     */
    public function setCamera(?string $camera): self
    {
        $this->camera = $camera;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLocale(): ?string
    {
        return $this->locale;
    }

    /**
     * @param string|null $locale
     */
    public function setLocale(?string $locale): self
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }

    /**
     * @param string|null $currencyCode
     */
    public function setCurrencyCode(?string $currencyCode): self
    {
        $this->currencyCode = $currencyCode;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getInstalledApplicationList(): ?array
    {
        return $this->installedApplicationList;
    }

    /**
     * @param array|null $installedApplicationList
     */
    public function setInstalledApplicationList(?array $installedApplicationList): self
    {
        $this->installedApplicationList = $installedApplicationList;
        return $this;
    }

}