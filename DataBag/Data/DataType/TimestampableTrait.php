<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 10/08/18
 * Time: 08:45
 */
namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType;

trait TimestampableTrait
{
    /**
     * @var \DateTime
     */
    protected $createdOn = null;
    /**
     * @var \DateTime
     */
    protected $updatedOn = null;

    /**
     * @return mixed
     */
    public function getCreatedOn(): ?\DateTime
    {
        return $this->createdOn;
    }

    /**
     * @param mixed $createdOn
     */
    public function setCreatedOn(?\DateTime $createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedOn(): ?\DateTime
    {
        return $this->updatedOn;
    }

    /**
     * @param mixed $updatedOn
     */
    public function setUpdatedOn(?\DateTime $updatedOn)
    {
        $this->updatedOn = $updatedOn;

        return $this;
    }
}