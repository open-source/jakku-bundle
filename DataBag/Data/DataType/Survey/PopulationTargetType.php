<?php
/**
 * Created by PhpStorm.
 * User: aboutgoods
 * Date: 08/11/18
 * Time: 10:04
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Survey;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;

class PopulationTargetType extends AbstractDataType
{
    private $id;
    private $created_on;
    private $numberOfUser;
    private $countries;
    private $languages;
    private $prequalificationList;
    private $sex;
    private $uuid;
    private $consumptionBehaviorList;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return PopulationTargetType
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedOn()
    {
        return $this->created_on;
    }

    /**
     * @param mixed $created_on
     * @return PopulationTargetType
     */
    public function setCreatedOn($created_on)
    {
        $this->created_on = $created_on;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumberOfUser()
    {
        return $this->numberOfUser;
    }

    /**
     * @param mixed $numberOfUser
     * @return PopulationTargetType
     */
    public function setNumberOfUser($numberOfUser)
    {
        $this->numberOfUser = $numberOfUser;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * @param mixed $countries
     * @return PopulationTargetType
     */
    public function setCountries($countries)
    {
        $this->countries = $countries;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @param mixed $languages
     * @return PopulationTargetType
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrequalificationList()
    {
        return $this->prequalificationList;
    }

    /**
     * @param mixed $prequalificationList
     * @return PopulationTargetType
     */
    public function setPrequalificationList($prequalificationList)
    {
        $this->prequalificationList = $prequalificationList;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param mixed $sex
     * @return PopulationTargetType
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     * @return PopulationTargetType
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getConsumptionBehaviorList()
    {
        return $this->consumptionBehaviorList;
    }

    /**
     * @param mixed $consumptionBehaviorList
     * @return PopulationTargetType
     */
    public function setConsumptionBehaviorList($consumptionBehaviorList)
    {
        $this->consumptionBehaviorList = $consumptionBehaviorList;
        return $this;
    }

    public function type(): string
    {
        return "POPULATION_TARGET";
    }
}