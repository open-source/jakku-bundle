<?php
/**
 * Created by PhpStorm.
 * User: aboutgoods
 * Date: 07/11/18
 * Time: 14:43
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Survey;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;

class AnswerType extends AbstractDataType
{
    protected $idS;
    protected $stamp;
    protected $skipped;
    protected $responses;

    /**
     * @return mixed
     */
    public function getIdS()
    {
        return $this->idS;
    }

    /**
     * @param mixed $idS
     * @return AnswerType
     */
    public function setIdS($idS)
    {
        $this->idS = $idS;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStamp()
    {
        return $this->stamp;
    }

    /**
     * @param mixed $stamp
     * @return AnswerType
     */
    public function setStamp($stamp)
    {
        $this->stamp = $stamp;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSkipped()
    {
        return $this->skipped;
    }

    /**
     * @param mixed $skipped
     * @return AnswerType
     */
    public function setSkipped($skipped)
    {
        $this->skipped = $skipped;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getResponses()
    {
        return $this->responses;
    }

    /**
     * @param mixed $responses
     * @return AnswerType
     */
    public function setResponses($responses)
    {
        $this->responses = $responses;
        return $this;
    }


    public function type(): string
    {
        return "ANSWER";
    }
}