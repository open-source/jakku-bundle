<?php
/**
 * Created by PhpStorm.
 * User: aboutgoods
 * Date: 08/11/18
 * Time: 10:44
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Survey;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;

class TriggerListType extends AbstractDataType
{
    private $type;
    private $action;
    private $parameterList;

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return TriggerListType
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param mixed $action
     * @return TriggerListType
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getParameterList()
    {
        return $this->parameterList;
    }

    /**
     * @param mixed $parameterList
     * @return TriggerListType
     */
    public function setParameterList($parameterList)
    {
        $this->parameterList = $parameterList;
        return $this;
    }

    public function type(): string
    {
        return "TRIGGER_LIST";
    }
}