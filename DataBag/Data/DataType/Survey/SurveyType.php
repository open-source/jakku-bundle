<?php
/**
 * Created by PhpStorm.
 * User: aboutgoods
 * Date: 08/11/18
 * Time: 10:09
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Survey;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;

class SurveyType extends AbstractDataType
{
    private $idS;
    private $title;
    private $user;
    private $startDate;
    private $endDate;
    private $questions;

    /**
     * @return mixed
     */
    public function getIdS()
    {
        return $this->idS;
    }

    /**
     * @param mixed $idS
     * @return SurveyType
     */
    public function setIdS($idS)
    {
        $this->idS = $idS;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return SurveyType
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return SurveyType
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param mixed $startDate
     * @return SurveyType
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param mixed $endDate
     * @return SurveyType
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * @param mixed $questions
     * @return SurveyType
     */
    public function setQuestions($questions)
    {
        $this->questions = $questions;
        return $this;
    }

    public function type(): string
    {
        return "SURVEY";
    }
}