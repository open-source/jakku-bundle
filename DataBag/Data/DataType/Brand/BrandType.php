<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 10/08/18
 * Time: 08:41
 */
namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Brand;

use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;

class BrandType extends AbstractDataType
{

    protected $name;
    protected $website;
    protected $logo;
    protected $description;
    protected $organization;
    protected $pointOfSaleList;
    protected $domainOfActivityList;
    protected $labelList;
    protected $locale;
    protected $retailType;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return BrandType
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param mixed $website
     * @return BrandType
     */
    public function setWebsite($website)
    {
        $this->website = $website;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param mixed $logo
     * @return BrandType
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return BrandType
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param mixed $organization
     * @return BrandType
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPointOfSaleList()
    {
        return $this->pointOfSaleList;
    }

    /**
     * @param mixed $pointOfSaleList
     * @return BrandType
     */
    public function setPointOfSaleList($pointOfSaleList)
    {
        $this->pointOfSaleList = $pointOfSaleList;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDomainOfActivityList()
    {
        return $this->domainOfActivityList;
    }

    /**
     * @param mixed $domainOfActivityList
     * @return BrandType
     */
    public function setDomainOfActivityList($domainOfActivityList)
    {
        $this->domainOfActivityList = $domainOfActivityList;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLabelList()
    {
        return $this->labelList;
    }

    /**
     * @param mixed $labelList
     * @return BrandType
     */
    public function setLabelList($labelList)
    {
        $this->labelList = $labelList;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param mixed $locale
     * @return BrandType
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRetailType()
    {
        return $this->retailType;
    }

    /**
     * @param mixed $retailType
     * @return BrandType
     */
    public function setRetailType($retailType)
    {
        $this->retailType = $retailType;
        return $this;
    }



    public function type(): string
    {
        return "BRAND";
    }


}