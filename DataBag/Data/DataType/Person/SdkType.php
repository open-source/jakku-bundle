<?php
/**
 * Created by PhpStorm.
 * User: aboutgoods
 * Date: 20/03/19
 * Time: 13:51
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;

class SdkType extends AbstractDataType
{

    /**
     * @var string|null
     */
    protected $versionBuild;
    /**
     * @var string|null
     */
    protected $versionName;
    /**
     * @var array|null
     */
    protected $configuration;

    /**
     * @return string|null
     */
    public function getVersionBuild(): ?string
    {
        return $this->versionBuild;
    }

    /**
     * @param string|null $versionBuild
     */
    public function setVersionBuild(?string $versionBuild): self
    {
        $this->versionBuild = $versionBuild;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getVersionName(): ?string
    {
        return $this->versionName;
    }

    /**
     * @param string|null $versionName
     */
    public function setVersionName(?string $versionName): self
    {
        $this->versionName = $versionName;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getConfiguration(): ?array
    {
        return $this->configuration;
    }

    /**
     * @param array|null $configuration
     */
    public function setConfiguration(?array $configuration): self
    {
        $this->configuration = $configuration;
        return $this;
    }


    public function type(): string
    {
        return "SDK";
    }
}