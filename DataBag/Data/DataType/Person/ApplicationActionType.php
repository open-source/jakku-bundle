<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 03/09/2018
 * Time: 10:29
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;

class ApplicationActionType extends AbstractDataType
{
    protected $application;
    protected $action;
    protected $date;

    /**
     * @return mixed
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * @param mixed $application
     * @return ApplicationActionType
     */
    public function setApplication($application)
    {
        $this->application = $application;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param mixed $action
     * @return ApplicationActionType
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     * @return ApplicationActionType
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    public function type(): string
    {
        return "APPLICATION_ACTION";
    }
}