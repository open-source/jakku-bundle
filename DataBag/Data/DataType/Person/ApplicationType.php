<?php

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person;

use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Source\DefaultEntityTrait;

class ApplicationType extends AbstractDataType {

    use DefaultEntityTrait;

    /**
     * @var null|string
     */
    protected $versionBuild;

    /**
     * @var null|string
     */
    protected $name;

    /**
     * @var null|string
     */
    protected $versionName;



    public function type(): string {
        return "APPLICATION";
    }

    /**
     * @return string|null
     */
    public function getVersionBuild(): ?string
    {
        return $this->versionBuild;
    }

    /**
     * @param string|null $versionBuild
     */
    public function setVersionBuild(?string $versionBuild): self
    {
        $this->versionBuild = $versionBuild;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getVersionName(): ?string
    {
        return $this->versionName;
    }

    /**
     * @param string|null $versionName
     */
    public function setVersionName(?string $versionName): self
    {
        $this->versionName = $versionName;
        return $this;
    }

}