<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 03/09/2018
 * Time: 10:18
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;

class SocioProfessionalCategoryType extends AbstractDataType
{
    /**
     * @var string|null
     */
    protected $uid;
    /**
     * @var string|null
     */
    protected $name;

    /**
     * @return null|string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param null|string $uid
     * @return SocioProfessionalCategoryType
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     * @return SocioProfessionalCategoryType
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }


    public function type(): string
    {
        return "SOCIO_PROFESSIONAL_CATEGORY";
    }
}