<?php

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person;

use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;

class PersonType extends AbstractDataType {
    /**
     * @var string
     */
	protected $username;

    /**
     * @var string
     */
	protected $firstname;

    /**
     * @var string
     */
	protected $lastname;

    /**
     * @var string
     */
	protected $pictureUrl;

    /**
     * @var int
     */
	protected $householdSize;

    /**
     * @var string
     */
    protected $uuid;

    /**
     * @var string
     */
    protected $eMail;

    /**
     * @var string
     */
    protected $birthDate;

    /**
     * @var string
     */
    protected $gender;

    /**
     * @var SocioProfessionalCategoryType
     */
    protected $socioProfessionalCategory;

    /**
     * @var string
     */
    protected $fromIp;

    /**
     * @var string
     */
    protected $locale;

    /**
     * @var string
     */
    protected $age;

    /**
     * @var string
     */
    protected $advertisingId;

    //This is an array from DW Perspective but since it's a databag per user, we consider that is it a single key -> value
    protected $externalUuid;

    /**
     * @return array
     */
    public function getExternalUuid(): ?array {
        return $this->externalUuid;
    }

    /**
     * @param mixed $externalUuid
     * @return PersonType
     * //This is an array from DW Perspective but since it's a databag per user, we consider that is it a single key -> value
     * Example :
     * externalUuid : {
     *      "devId-{devId}" : "1234"
     *          }
     */
    public function setExternalUuid(?array $externalUuid): self {
        $this->externalUuid = $externalUuid;
        return $this;
    }

    /**
     * @return string
     */
    public function getUuid(): string {
        return $this->uuid;
    }

    /**
     * @param string|null $uuid
     * @return PersonType
     */
    public function setUuid(?string $uuid): self {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * @return string
     */
    public function getEMail(): ?string {
        return $this->eMail;
    }

    /**
     * @param string|null $eMail
     * @return PersonType
     */
    public function setEMail(?string $eMail): self {
        $this->eMail = $eMail;
        return $this;
    }

    /**
     * @return string
     */
    public function getBirthDate(): ?string {
        return $this->birthDate;
    }

    /**
     * @param string|null $birthDate
     * @return PersonType
     */
    public function setBirthDate(?string $birthDate): self {
        $this->birthDate = $birthDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getGender(): ?string {
        return $this->gender;
    }

    /**
     * @param string|null $gender
     * @return PersonType
     */
    public function setGender(?string $gender): self {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return SocioProfessionalCategoryType
     */
    public function getSocioProfessionalCategory(): ?SocioProfessionalCategoryType {
        return $this->socioProfessionalCategory;
    }

    /**
     * @param SocioProfessionalCategoryType $socioProfessionalCategory
     * @return PersonType
     */
    public function setSocioProfessionalCategory(SocioProfessionalCategoryType $socioProfessionalCategory): self {
        $this->socioProfessionalCategory = $socioProfessionalCategory;
        return $this;
    }

    /**
     * @return string
     */
    public function getFromIp(): ?string {
        return $this->fromIp;
    }

    /**
     * @param string|null $fromIp
     * @return PersonType
     */
    public function setFromIp(?string $fromIp): self {
        $this->fromIp = $fromIp;
        return $this;
    }

    /**
     * @return string
     */
    public function getLocale(): ?string {
        return $this->locale;
    }

    /**
     * @param string|null $locale
     * @return PersonType
     */
    public function setLocale(?string $locale): self {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return string
     */
    public function getAge(): ?string {
        return $this->age;
    }

    /**
     * @param string|null $age
     * @return PersonType
     */
    public function setAge(?string $age): self {
        $this->age = $age;
        return $this;
    }

    /**
     * @return string
     */
    public function getAdvertisingId(): ?string {
        return $this->advertisingId;
    }

    /**
     * @param string|null $advertisingId
     * @return PersonType
     */
    public function setAdvertisingId(?string $advertisingId): self {
        $this->advertisingId = $advertisingId;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string|null $username
     *
     * @return PersonType
     */
    public function setUsername(?string $username): self {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param string|null $firstname
     *
     * @return PersonType
     */
    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @param string|null $lastname
     *
     * @return PersonType
     */
    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * @return string
     */
    public function getPictureUrl(): ?string
    {
        return $this->pictureUrl;
    }

    /**
     * @param string|null $pictureUrl
     *
     * @return PersonType
     */
    public function setPictureUrl(?string $pictureUrl):self
    {
        $this->pictureUrl = $pictureUrl;
        return $this;
    }

    /**
     * @return int
     */
    public function getHouseholdSize(): ?int
    {
        return $this->householdSize;
    }

    /**
     * @param int $houseHoldSize
     *
     * @return PersonType
     */
    public function setHouseholdSize(?int $houseHoldSize):self
    {
        $this->householdSize = $houseHoldSize;
        return $this;
    }

    public function type(): string {
        return "PERSON";
    }

}