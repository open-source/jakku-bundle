<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 03/09/2018
 * Time: 10:29
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person;

use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Source\SourceTrait;

class ActionType extends AbstractDataType
{
    use SourceTrait;
    /**
     * @var string|null
     */
    protected $key;
    /**
     * @var string|null
     */
    protected $value;

    /**
     * @return null|string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param null|string $key
     * @return ActionType
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return null|array
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param null|array $value
     * @return ActionType
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    public function type(): string
    {
        return "ACTION";
    }
}