<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 10/08/18
 * Time: 11:49
 */
namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType;

use Ramsey\Uuid\Uuid;

class GeoType extends AbstractDataType
{
    protected $idGeo;
    /**
     * @var float
     */
    protected $longitude;
    /**
     * @var float
     */
    protected $latitude;

    public function type(): string
    {
        return "GEO";
    }

    public function __construct($latitude, $longitude)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->idGeo = Uuid::uuid5(Uuid::NAMESPACE_URL, "geo://".$this->latitude."/".$this->longitude);
    }

    /**
     * @return mixed
     */
    public function getIdGeo()
    {
        return $this->idGeo;
    }

    /**
     * @param mixed $idGeo
     */
    public function setIdGeo($idGeo)
    {
        $this->idGeo = $idGeo;

        return $this;
    }

    /**
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     */
    public function setLongitude(float $longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }

    /**
     * @param float $latitude
     */
    public function setLatitude(float $latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }


}