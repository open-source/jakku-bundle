<?php
/**
 * Created by PhpStorm.
 * User: aboutgoods
 * Date: 03/12/18
 * Time: 11:20
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Ocr;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;

class OcrOutputVisionDataType extends AbstractDataType
{
    protected $full;

    //@var array|null
    protected $blocks;
    protected $ocrType;
    protected $analysisDate;
    protected $dominantLanguage;

    /**
     * @return mixed
     */
    public function getFull()
    {
        return $this->full;
    }

    /**
     * @param mixed $full
     * @return OcrOutputVisionDataType
     */
    public function setFull($full)
    {
        $this->full = $full;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBlocks()
    {
        return $this->blocks;
    }

    /**
     * @param mixed $blocks
     * @return OcrOutputVisionDataType
     */
    public function setBlocks($blocks)
    {
        $this->blocks = $blocks;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOcrType()
    {
        return $this->ocrType;
    }

    /**
     * @param mixed $ocrType
     * @return OcrOutputVisionDataType
     */
    public function setOcrType($ocrType)
    {
        $this->ocrType = $ocrType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAnalysisDate()
    {
        return $this->analysisDate;
    }

    /**
     * @param mixed $analysisDate
     * @return OcrOutputVisionDataType
     */
    public function setAnalysisDate($analysisDate)
    {
        $this->analysisDate = $analysisDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDominantLanguage()
    {
        return $this->dominantLanguage;
    }

    /**
     * @param mixed $dominantLanguage
     * @return OcrOutputVisionDataType
     */
    public function setDominantLanguage($dominantLanguage)
    {
        $this->dominantLanguage = $dominantLanguage;
        return $this;
    }



    public function type(): string
    {
        return "OCR_OUTPUT";
    }
}