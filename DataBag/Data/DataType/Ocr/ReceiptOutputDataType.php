<?php
/**
 * Created by PhpStorm.
 * User: aboutgoods
 * Date: 03/12/18
 * Time: 11:20
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Ocr;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;

class ReceiptOutputDataType extends AbstractDataType
{

    protected $uid;
    protected $name;
    protected $data;
    protected $version;
    protected $campaignId;

    /**
     * @return mixed
     */
    public function getCampaignId():?string
    {
        return $this->campaignId;
    }

    /**
     * @param mixed $campaignId
     */
    public function setCampaignId($campaignId)
    {
        $this->campaignId = $campaignId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     *
     * @return ReceiptOutputDataType
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return ReceiptOutputDataType
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     *
     * @return ReceiptOutputDataType
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     *
     * @return ReceiptOutputDataType
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    public function type(): string
    {
        return "RECEIPT_OUTPUT";
    }
}