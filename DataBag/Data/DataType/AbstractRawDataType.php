<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 10/08/18
 * Time: 10:18
 */
namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType;

abstract class AbstractRawDataType extends AbstractDataType implements \JsonSerializable
{
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function jsonSerialize()
    {
        return $this->value;
    }
}