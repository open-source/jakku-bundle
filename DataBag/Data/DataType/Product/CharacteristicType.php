<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 03/09/2018
 * Time: 10:05
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Product;


class CharacteristicType
{
    /**
     * @var string|null
     */
    protected $name;
    /**
     * @var CharacteristicType|null
     */
    protected $parentCharacteristic;

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return CharacteristicType|null
     */
    public function getParentCharacteristic()
    {
        return $this->parentCharacteristic;
    }

    /**
     * @param CharacteristicType|null $parentCharacteristic
     */
    public function setParentCharacteristic($parentCharacteristic)
    {
        $this->parentCharacteristic = $parentCharacteristic;
        return $this;
    }

}