<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 03/09/2018
 * Time: 10:10
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Product;


class ColorType
{
    /**
     * @var string|null
     */
    protected $name;
    /**
     * @var ColorType|null
     */
    protected $parentColor;

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return ColorType|null
     */
    public function getParentColor()
    {
        return $this->parentColor;
    }

    /**
     * @param ColorType|null $parentColor
     */
    public function setParentColor($parentColor)
    {
        $this->parentColor = $parentColor;
        return $this;
    }

}