<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 03/09/2018
 * Time: 09:19
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Product;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Brand\BrandType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Source\DefaultEntityTrait;

class ProductCategoryType
{
    use DefaultEntityTrait;

    protected $name;
    protected $idGs1;
    /**
     * @var ProductCategoryType|null
     */
    protected $parentProductCategory;
    /**
     * @var KeycatType|null
     */
    protected $keycat;
    /**
     * @var SystemItemType|null
     */
    protected $systemItem;
    /**
     * @var BrandType[]|null
     */
    protected $brandList;
    /**
     * @var FlavourType[]|null
     */
    protected $flavourList;
    /**
     * @var CharacteristicType[]|null
     */
    protected $characteristicList;
    /**
     * @var PackagingType[]|null
     */
    protected $packagingList;
    /**
     * @var BundleType[]|null
     */
    protected $bundleList;
    /**
     * @var ColorType[]|null
     */
    protected $colorList;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdGs1()
    {
        return $this->idGs1;
    }

    /**
     * @param mixed $idGs1
     */
    public function setIdGs1($idGs1)
    {
        $this->idGs1 = $idGs1;
        return $this;
    }

    /**
     * @return ProductCategoryType|null
     */
    public function getParentProductCategory()
    {
        return $this->parentProductCategory;
    }

    /**
     * @param ProductCategoryType|null $parentProductCategory
     */
    public function setParentProductCategory($parentProductCategory)
    {
        $this->parentProductCategory = $parentProductCategory;
        return $this;
    }

    /**
     * @return KeycatType|null
     */
    public function getKeycat()
    {
        return $this->keycat;
    }

    /**
     * @param KeycatType|null $keycat
     */
    public function setKeycat($keycat)
    {
        $this->keycat = $keycat;
        return $this;
    }

    /**
     * @return SystemItemType|null
     */
    public function getSystemItem()
    {
        return $this->systemItem;
    }

    /**
     * @param SystemItemType|null $systemItem
     */
    public function setSystemItem($systemItem)
    {
        $this->systemItem = $systemItem;
        return $this;
    }

    /**
     * @return BrandType[]|null
     */
    public function getBrandList()
    {
        return $this->brandList;
    }

    /**
     * @param BrandType[]|null $brandList
     */
    public function setBrandList($brandList)
    {
        $this->brandList = $brandList;
        return $this;
    }

    /**
     * @return FlavourType[]|null
     */
    public function getFlavourList()
    {
        return $this->flavourList;
    }

    /**
     * @param FlavourType[]|null $flavourList
     */
    public function setFlavourList($flavourList)
    {
        $this->flavourList = $flavourList;
        return $this;
    }

    /**
     * @return CharacteristicType[]|null
     */
    public function getCharacteristicList()
    {
        return $this->characteristicList;
    }

    /**
     * @param CharacteristicType[]|null $characteristicList
     */
    public function setCharacteristicList($characteristicList)
    {
        $this->characteristicList = $characteristicList;
        return $this;
    }

    /**
     * @return PackagingType[]|null
     */
    public function getPackagingList()
    {
        return $this->packagingList;
    }

    /**
     * @param PackagingType[]|null $packagingList
     */
    public function setPackagingList($packagingList)
    {
        $this->packagingList = $packagingList;
        return $this;
    }

    /**
     * @return BundleType[]|null
     */
    public function getBundleList()
    {
        return $this->bundleList;
    }

    /**
     * @param BundleType[]|null $bundleList
     */
    public function setBundleList($bundleList)
    {
        $this->bundleList = $bundleList;
        return $this;
    }

    /**
     * @return ColorType[]|null
     */
    public function getColorList()
    {
        return $this->colorList;
    }

    /**
     * @param ColorType[]|null $colorList
     */
    public function setColorList($colorList)
    {
        $this->colorList = $colorList;
        return $this;
    }

}