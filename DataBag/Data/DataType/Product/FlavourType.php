<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 03/09/2018
 * Time: 10:04
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Product;


class FlavourType
{
    /**
     * @var string|null
     */
    protected $name;
    /**
     * @var FlavourType|null
     */
    protected $parentFlavour;

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return FlavourType|null
     */
    public function getParentFlavour()
    {
        return $this->parentFlavour;
    }

    /**
     * @param FlavourType|null $parentFlavour
     */
    public function setParentFlavour($parentFlavour)
    {
        $this->parentFlavour = $parentFlavour;
        return $this;
    }
}