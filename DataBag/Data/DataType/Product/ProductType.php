<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 10/08/18
 * Time: 08:42
 */
namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Product;

use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Brand\BrandType;

class ProductType extends AbstractDataType
{
    /**
     * @var string|null
     */
    protected $label;
    /**
     * @var string|null
     */
    protected $idGs1;
    /**
     * @var string|null
     */
    protected $categoryUid;
    /**
     * @var string|null
     */
    protected $category;
    /**
     * @var BrandType|null
     */
    protected $brand;
    /**
     * @var string|null
     */
    protected $brandLine;

    /**
     * @return string|null
     */
    public function getBrandLine()
    {
        return $this->brandLine;
    }

    /**
     * @param string|null $brandLine
     *
     * @return ProductType
     */
    public function setBrandLine(?string $brandLine)
    {
        $this->brandLine = $brandLine;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string|null $label
     *
     * @return ProductType
     */
    public function setLabel(?string $label): ProductType
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getIdGs1()
    {
        return $this->idGs1;
    }

    /**
     * @param string|null $idGs1
     *
     * @return ProductType
     */
    public function setIdGs1(?string $idGs1): ProductType
    {
        $this->idGs1 = $idGs1;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCategoryUid()
    {
        return $this->categoryUid;
    }

    /**
     * @param string|null $categoryUid
     *
     * @return ProductType
     */
    public function setCategoryUid(?string $categoryUid): ProductType
    {
        $this->categoryUid = $categoryUid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string|null $category
     *
     * @return ProductType
     */
    public function setCategory(?string $category): ProductType
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return BrandType|null
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param BrandType|null $brand
     *
     * @return ProductType
     */
    public function setBrand(BrandType $brand): ProductType
    {
        $this->brand = $brand;
        return $this;
    }

    public function type(): string
    {
        return "PRODUCT";
    }

}