<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 03/09/2018
 * Time: 10:36
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Product;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Acronym\AcronymType;

class SystemItemAcronymType
{
    /**
     * @var SystemItemType|null
     */
    protected $systemItem;
    /**
     * @var AcronymType|null
     */
    protected $acronym;
    protected $locale;

    /**
     * @return SystemItemType|null
     */
    public function getSystemItem()
    {
        return $this->systemItem;
    }

    /**
     * @param SystemItemType|null $systemItem
     */
    public function setSystemItem($systemItem)
    {
        $this->systemItem = $systemItem;
        return $this;
    }

    /**
     * @return AcronymType|null
     */
    public function getAcronym()
    {
        return $this->acronym;
    }

    /**
     * @param AcronymType|null $acronym
     */
    public function setAcronym($acronym)
    {
        $this->acronym = $acronym;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param mixed $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }

}