<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 03/09/2018
 * Time: 10:01
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Product;


class SystemItemType
{
    protected $name = null;
    /**
     * @var SystemItemAcronymType[]|null
     */
    protected $systemItemAcronymList = null;

    /**
     * @return null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return SystemItemAcronymType[]|null
     */
    public function getSystemItemAcronymList()
    {
        return $this->systemItemAcronymList;
    }

    /**
     * @param SystemItemAcronymType[]|null $systemItemAcronymList
     */
    public function setSystemItemAcronymList($systemItemAcronymList)
    {
        $this->systemItemAcronymList = $systemItemAcronymList;
        return $this;
    }


}