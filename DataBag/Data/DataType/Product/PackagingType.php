<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 03/09/2018
 * Time: 10:07
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Product;


class PackagingType
{
    /**
     * @var string|null
     */
    protected $name;
    /**
     * @var PackagingType|null
     */
    protected $parentPackaging;

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return PackagingType|null
     */
    public function getParentPackaging()
    {
        return $this->parentPackaging;
    }

    /**
     * @param PackagingType|null $parentPackaging
     */
    public function setParentPackaging($parentPackaging)
    {
        $this->parentPackaging = $parentPackaging;
        return $this;
    }

}