<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 03/09/2018
 * Time: 10:08
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Product;


class BundleType
{
    /**
     * @var string|null
     */
    protected $name;
    /**
     * @var BundleType|null
     */
    protected $parentBundle;

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return BundleType|null
     */
    public function getParentBundle()
    {
        return $this->parentBundle;
    }

    /**
     * @param BundleType|null $parentBundle
     */
    public function setParentBundle($parentBundle)
    {
        $this->parentBundle = $parentBundle;
        return $this;
    }

}