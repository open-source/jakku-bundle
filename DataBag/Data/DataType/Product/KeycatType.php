<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 03/09/2018
 * Time: 09:59
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Product;


class KeycatType
{
    /**
     * @var string|null
     */
    protected $targetCode;
    /**
     * @var string|null
     */
    protected $description;

    /**
     * @return null|string
     */
    public function getTargetCode()
    {
        return $this->targetCode;
    }

    /**
     * @param null|string $targetCode
     */
    public function setTargetCode($targetCode)
    {
        $this->targetCode = $targetCode;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }
}