<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 03/09/2018
 * Time: 10:14
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\TradeAction;


class CurrencyType
{
    /**
     * @var string|null
     */
    protected $symbol;
    /**
     * @var string|null
     */
    protected $label;
    /**
     * @var string|null
     */
    protected $locale;
    /**
     * @var string|null
     */
    protected $iso4217Code;

    /**
     * @return null|string
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * @param null|string $symbol
     */
    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param null|string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param null|string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getIso4217Code()
    {
        return $this->iso4217Code;
    }

    /**
     * @param null|string $iso4217Code
     */
    public function setIso4217Code($iso4217Code)
    {
        $this->iso4217Code = $iso4217Code;
        return $this;
    }

}