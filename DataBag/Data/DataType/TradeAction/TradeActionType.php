<?php
/**
 * Created by PhpStorm.
 * User: jeremy
 * Date: 10/08/2018
 * Time: 15:51
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\TradeAction;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\PointOfSale\OrganizationType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person\ActionType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\PointOfSale\PointOfSaleType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Source\DefaultEntityTrait;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Source\SourceTrait;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Source\SourceType;

class TradeActionType extends AbstractDataType
{

    use SourceTrait;
    use DefaultEntityTrait;

    /**
     * @var OrganizationType|null
     */
    protected $organization;

    /**
     * @var ActionType|null
     */
    protected $action;

    /**
     * @var PointOfSaleType|null
     */
    protected $pointOfSale;

    /**
     * @var string|null
     */
    protected $fromClient;

    /**
     * @var String|null
     */
    protected $locale;

    /**
     * @var string[]
     */
    protected $instruments = [];

    /**
     * @var string|null
     */
    protected $settlementDate;

    /**
     * @var string|null
     */
    protected $tradeDate;

    /**
     * @var string|null
     */
    protected $tradeHour;

    /**
     * @var string|null
     */
    protected $status;
    /**
     * @var ReceiptType|null
     */
    protected $receipt;
    /**
     * @var TransactionType|null
     */
    protected $bankTransaction;

    /**
     * @var CurrencyType|null
     */
    protected $currency;

    /**
     * @var string|null
     */
    protected $amount;

    /**
     * @return SourceType|null
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param SourceType|null $source
     *
     * @return TradeActionType
     */
    public function setSource(SourceType $source): TradeActionType
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return OrganizationType|null
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param OrganizationType|null $organization
     *
     * @return TradeActionType
     */
    public function setOrganization(OrganizationType $organization): TradeActionType
    {
        $this->organization = $organization;
        return $this;
    }

    /**
     * @return ActionType|null
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param ActionType|null $action
     *
     * @return TradeActionType
     */
    public function setAction(ActionType $action): TradeActionType
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return PointOfSaleType|null
     */
    public function getPointOfSale()
    {
        return $this->pointOfSale;
    }

    /**
     * @param PointOfSaleType|null $pointOfSale
     *
     * @return TradeActionType
     */
    public function setPointOfSale(PointOfSaleType $pointOfSale): TradeActionType
    {
        $this->pointOfSale = $pointOfSale;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFromClient()
    {
        return $this->fromClient;
    }

    /**
     * @param string|null $fromClient
     *
     * @return TradeActionType
     */
    public function setFromClient(?string $fromClient): TradeActionType
    {
        $this->fromClient = $fromClient;
        return $this;
    }

    /**
     * @return String|null
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param String|null $locale
     *
     * @return TradeActionType
     */
    public function setLocale(?String $locale): TradeActionType
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return string[]|null
     */
    public function getInstruments()
    {
        return $this->instruments;
    }

    /**
     * @param string|null $instrument
     * @return TradeActionType
     */
    public function addInstrument($instrument)
    {
        $this->instruments[] = $instrument;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSettlementDate()
    {
        return $this->settlementDate;
    }

    /**
     * @param mixed $settlementDate
     *
     * @return TradeActionType
     */
    public function setSettlementDate($settlementDate)
    {
        $this->settlementDate = $settlementDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTradeDate()
    {
        return $this->tradeDate;
    }

    /**
     * @param mixed $tradeDate
     *
     * @return TradeActionType
     */
    public function setTradeDate($tradeDate)
    {
        $this->tradeDate = $tradeDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTradeHour()
    {
        return $this->tradeHour;
    }

    /**
     * @param mixed $tradeHour
     *
     * @return TradeActionType
     */
    public function setTradeHour($tradeHour)
    {
        $this->tradeHour = $tradeHour;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     *
     * @return TradeActionType
     */
    public function setStatus(?string $status): TradeActionType
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return ReceiptType|null
     */
    public function getReceipt()
    {
        return $this->receipt;
    }

    /**
     * @param ReceiptType|null $receipt
     *
     * @return TradeActionType
     */
    public function setReceipt(ReceiptType $receipt): TradeActionType
    {
        $this->receipt = $receipt;
        return $this;
    }

    /**
     * @return Transactiontype|null
     */
    public function getBankTransaction()
    {
        return $this->bankTransaction;
    }

    /**
     * @param Transactiontype|null $bankTransaction
     *
     * @return TradeActionType
     */
    public function setBankTransaction(Transactiontype $bankTransaction): TradeActionType
    {
        $this->bankTransaction = $bankTransaction;
        return $this;
    }

    /**
     * @return CurrencyType|null
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param CurrencyType|null $currency
     *
     * @return TradeActionType
     */
    public function setCurrency(CurrencyType $currency): TradeActionType
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param string|null $amount
     *
     * @return TradeActionType
     */
    public function setAmount(?string $amount): TradeActionType
    {
        $this->amount = $amount;
        return $this;
    }

    public function type(): string
    {
        return "TRADE_ACTION";
    }


}