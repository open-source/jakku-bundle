<?php


namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\TradeAction;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;

class Transactiontype extends AbstractDataType
{
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $label;
    /**
     * @var string
     */
    protected $amount;
    /**
     * @var string
     */
    protected $type;
    /**
     * @var string
     */
    protected $currency;
    /**
     * @var string
     */
    protected $operationDate;
    /**
     * @var int
     */
    protected $shopId;
    /**
     * @var string
     */
    protected $countryCode;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Transactiontype
     */
    public function setId(int $id): Transactiontype
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string|null $label
     *
     * @return Transactiontype
     */
    public function setLabel(?string $label): Transactiontype
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param string|null $amount
     *
     * @return Transactiontype
     */
    public function setAmount(?string $amount): Transactiontype
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     *
     * @return Transactiontype
     */
    public function setType(?string $type): Transactiontype
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string|null $currency
     *
     * @return Transactiontype
     */
    public function setCurrency(?string $currency): Transactiontype
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return string
     */
    public function getOperationDate()
    {
        return $this->operationDate;
    }

    /**
     * @param string|null $operationDate
     *
     * @return Transactiontype
     */
    public function setOperationDate(?string $operationDate): Transactiontype
    {
        $this->operationDate = $operationDate;
        return $this;
    }

    /**
     * @return int
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * @param int $shopId
     *
     * @return Transactiontype
     */
    public function setShopId(?int $shopId): Transactiontype
    {
        $this->shopId = $shopId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * @param string|null $countryCode
     *
     * @return Transactiontype
     */
    public function setCountryCode(?string $countryCode): Transactiontype
    {
        $this->countryCode = $countryCode;
        return $this;
    }
    public function type(): string
    {
        return "TRANSACTION";
    }
}