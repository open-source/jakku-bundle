<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 03/09/2018
 * Time: 09:17
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\TradeAction;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Product\ProductType;

class ProductTradeActionType extends AbstractDataType
{
    /**
     * @var string|null
     */
    protected $uid;

    /**
     * @var string|null
     */
    protected $rawLabel;

    /**
     * @var string|null
     */
    protected $shortLabel;

    /**
     * @var string|null
     */
    protected $quantity;

    /**
     * @var string|null
     */
    protected $unitPrice;

    /**
     * @var string|null
     */
    protected $packageUnity;

    /**
     * @var string|null
     */
    protected $mass;

    /**
     * @var array[]|null
     */
    protected $flags;

    /**
     * @var string|null
     */
    protected $price;

    /**
     * @var ProductType|null
     */
    protected $product;

    /**
     * @var string|null
     */
    protected $locale;

    /**
     * @return string|null
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param string|null $uid
     *
     * @return ProductTradeActionType
     */
    public function setUid(?string $uid): ProductTradeActionType
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRawLabel()
    {
        return $this->rawLabel;
    }

    /**
     * @param string|null $rawLabel
     *
     * @return ProductTradeActionType
     */
    public function setRawLabel(?string $rawLabel): ProductTradeActionType
    {
        $this->rawLabel = $rawLabel;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getShortLabel()
    {
        return $this->shortLabel;
    }

    /**
     * @param string|null $shortLabel
     *
     * @return ProductTradeActionType
     */
    public function setShortLabel(?string $shortLabel): ProductTradeActionType
    {
        $this->shortLabel = $shortLabel;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param string|null $quantity
     *
     * @return ProductTradeActionType
     */
    public function setQuantity(?string $quantity): ProductTradeActionType
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * @param string|null $unitPrice
     *
     * @return ProductTradeActionType
     */
    public function setUnitPrice(?string $unitPrice): ProductTradeActionType
    {
        $this->unitPrice = $unitPrice;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPackageUnity()
    {
        return $this->packageUnity;
    }

    /**
     * @param string|null $packageUnity
     *
     * @return ProductTradeActionType
     */
    public function setPackageUnity(?string $packageUnity): ProductTradeActionType
    {
        $this->packageUnity = $packageUnity;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMass()
    {
        return $this->mass;
    }

    /**
     * @param string|null $mass
     *
     * @return ProductTradeActionType
     */
    public function setMass(?string $mass): ProductTradeActionType
    {
        $this->mass = $mass;
        return $this;
    }

    /**
     * @return array[]|null
     */
    public function getFlags()
    {
        return $this->flags;
    }

    /**
     * @param array[]|null $flags
     *
     * @return ProductTradeActionType
     */
    public function setFlags(?array $flags): ProductTradeActionType
    {
        $this->flags = $flags;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param string|null $price
     *
     * @return ProductTradeActionType
     */
    public function setPrice(?string $price): ProductTradeActionType
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return ProductType|null
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param ProductType|null $product
     *
     * @return ProductTradeActionType
     */
    public function setProduct(ProductType $product): ProductTradeActionType
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string|null $locale
     *
     * @return ProductTradeActionType
     */
    public function setLocale(?string $locale): ProductTradeActionType
    {
        $this->locale = $locale;
        return $this;
    }

    public function type(): string
    {
        return "PRODUCT_TRADE_ACTION";
    }
}