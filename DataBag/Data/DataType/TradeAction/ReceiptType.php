<?php


namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\TradeAction;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;

class ReceiptType extends AbstractDataType
{
    /**
     * @var string
     */
    protected $url;
    /**
     * @var int
     */
    protected $articleCount;
    /**
     * @var string
     */
    protected $barcode;
    /**
     * @var string
     */
    protected $uid;
    /**
     * @var string
     */
    protected $name;
    /**
     * @var string
     */
    protected $total;
    /**
     * @var string
     */
    protected $currency;
    /**
     * @var string
     */
    protected $locale;
    /**
     * @var string
     */
    protected $clientUniqueName;

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string|null $url
     *
     * @return ReceiptType
     */
    public function setUrl(?string $url): ReceiptType
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return int
     */
    public function getArticleCount()
    {
        return $this->articleCount;
    }

    /**
     * @param int $articleCount
     *
     * @return ReceiptType
     */
    public function setArticleCount(int $articleCount): ReceiptType
    {
        $this->articleCount = $articleCount;
        return $this;
    }

    /**
     * @return string
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @param string|null $barcode
     *
     * @return ReceiptType
     */
    public function setBarcode(?string $barcode): ReceiptType
    {
        $this->barcode = $barcode;
        return $this;
    }

    /**
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     *
     * @return ReceiptType
     */
    public function setUid(string $uid): ReceiptType
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return ReceiptType
     */
    public function setName(string $name): ReceiptType
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param string|null $total
     *
     * @return ReceiptType
     */
    public function setTotal(?string $total): ReceiptType
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string|null $currency
     *
     * @return ReceiptType
     */
    public function setCurrency(?string $currency): ReceiptType
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string|null $locale
     *
     * @return ReceiptType
     */
    public function setLocale(?string $locale): ReceiptType
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return string
     */
    public function getClientUniqueName()
    {
        return $this->clientUniqueName;
    }

    /**
     * @param string|null $clientUniqueName
     *
     * @return ReceiptType
     */
    public function setClientUniqueName(?string $clientUniqueName): ReceiptType
    {
        $this->clientUniqueName = $clientUniqueName;
        return $this;
    }

    public function type(): string
    {
        return "RECEIPT";
    }
}