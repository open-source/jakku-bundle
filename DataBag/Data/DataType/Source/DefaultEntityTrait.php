<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 13/12/2018
 * Time: 10:47
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Source;


trait DefaultEntityTrait
{
    protected $uid;

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
        return $this;
    }

}