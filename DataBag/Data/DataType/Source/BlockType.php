<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 10/09/2018
 * Time: 08:34
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Source;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;

class BlockType extends AbstractDataType
{
    protected $type;
    protected $image;
    protected $x;
    protected $y;

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @param mixed $x
     */
    public function setX($x)
    {
        $this->x = $x;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @param mixed $y
     */
    public function setY($y)
    {
        $this->y = $y;
        return $this;
    }


    
    public static function getTypes()
    {
        return [
            "Entête" => "HEADER",
            "Bas de ticket" => "FOOTER",
            "Prix" => "PRICE",
            "Logo" => "LOGO",
            "Ticket" => "RECEIPT",
            "Libellés produits" => "PRODUCT",
            "Code barre" => "BARCODE"

        ];
    }

    public function type(): string
    {
        return "BLOCK";
    }
}