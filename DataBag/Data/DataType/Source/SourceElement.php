<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 04/09/2018
 * Time: 08:58
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Source;


class SourceElement
{
    /**
     * @var string|null
     */
    protected $type;
    /**
     * @var string|null
     */
    protected $uid;
    /**
     * @var ImageType|null
     */
    protected $image;

    /**
     * @return null|string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param null|string $type
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param null|string $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @return ImageType|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param ImageType|null $image
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }


}