<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 10/08/18
 * Time: 10:59
 */
namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Source;


trait SourceTrait
{
    /**
     * @var SourceType|null
     */
    protected $source;

    /**
     * @return SourceType|null
     */
    public function getSource(): ?SourceType
    {
        return $this->source;
    }

    /**
     * @param SourceType|null $source
     */
    public function setSource(?SourceType $source)
    {
        $this->source = $source;

        return $this;
    }

}