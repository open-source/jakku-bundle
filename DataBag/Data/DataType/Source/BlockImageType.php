<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 10/09/2018
 * Time: 08:30
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Source;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;

class BlockImageType extends AbstractDataType
{
    /**
     * @var BlockType|null
     */
    protected $block;

    /**
     * @var ImageType|null
     */
    protected $image;

    protected $x;

    protected $y;

    /**
     * @return BlockType|null
     */
    public function getBlock()
    {
        return $this->block;
    }

    /**
     * @param BlockType|null $block
     */
    public function setBlock($block)
    {
        $this->block = $block;
        return $this;
    }

    /**
     * @return ImageType|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param ImageType|null $image
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @param mixed $x
     */
    public function setX($x)
    {
        $this->x = $x;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @param mixed $y
     */
    public function setY($y)
    {
        $this->y = $y;
        return $this;
    }

    public function type(): string
    {
        return "BLOCK_IMAGE";
    }
}