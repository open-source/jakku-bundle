<?php
/**
 * Created by PhpStorm.
 * User: gweltaz
 * Date: 04/09/2018
 * Time: 09:00
 */

namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Source;

use Aboutgoods\JakkuBundle\DataBag\Data\DataType\TimestampableTrait;

class SourceType
{
    use TimestampableTrait;

    /**
     * @var string|null
     */
    protected $uid;
    /**
     * @var string|null
     */
    protected $fromProject;
    /**
     * @var string|null
     */
    protected $fromUrl;
    /**
     * @var string|null
     */
    protected $fromProjectVersion;
    /**
     * @var string|null
     */
    protected $projectInfos;

    /**
     * @var SourceElement[]|null
     */
    protected $sourceElementList;

    /**
     * @return null|string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param null|string $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getFromProject()
    {
        return $this->fromProject;
    }

    /**
     * @param null|string $fromProject
     */
    public function setFromProject($fromProject)
    {
        $this->fromProject = $fromProject;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getFromUrl()
    {
        return $this->fromUrl;
    }

    /**
     * @param null|string $fromUrl
     */
    public function setFromUrl($fromUrl)
    {
        $this->fromUrl = $fromUrl;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getFromProjectVersion()
    {
        return $this->fromProjectVersion;
    }

    /**
     * @param null|string $fromProjectVersion
     */
    public function setFromProjectVersion($fromProjectVersion)
    {
        $this->fromProjectVersion = $fromProjectVersion;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getProjectInfos()
    {
        return $this->projectInfos;
    }

    /**
     * @param null|string $projectInfos
     */
    public function setProjectInfos($projectInfos)
    {
        $this->projectInfos = $projectInfos;
        return $this;
    }

    /**
     * @return SourceElement[]|null
     */
    public function getSourceElementList()
    {
        return $this->sourceElementList;
    }

    /**
     * @param SourceElement[]|null $sourceElementList
     */
    public function setSourceElementList($sourceElementList)
    {
        $this->sourceElementList = $sourceElementList;
        return $this;
    }

}