<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 09/08/18
 * Time: 14:58
 */
namespace Aboutgoods\JakkuBundle\DataBag\Data\DataType\Source;

use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;

class ImageType extends AbstractDataType
{
    /**
     * @var string|null
     */
    protected $mime;
    /**
     * @var string|null
     */
    protected $url;
    /**
     * @var string|null
     */
    protected $hash;
    /**
     * @var string|null
     */
    protected $uid;

    /**
     * @var string[]|null
     */
    protected $metaData;

    /**
     * @return null|string
     */
    public function getMime()
    {
        return $this->mime;
    }

    /**
     * @param null|string $mime
     */
    public function setMime($mime)
    {
        $this->mime = $mime;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param null|string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param null|string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param null|string $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * @return null|string[]
     */
    public function getMetaData()
    {
        return $this->metaData;
    }

    /**
     * @param null|string[] $metaData
     */
    public function setMetaData($metaData)
    {
        $this->metaData = $metaData;

        return $this;
    }

    public function type(): string
    {
        return "IMAGE";
    }

}