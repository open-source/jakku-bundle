<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 09/08/18
 * Time: 16:32
 */
namespace Aboutgoods\JakkuBundle\DataBag;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\PointOfSale\OrganizationType;

class Organization extends AbstractDataBag
{
    public function getBagType()
    {
        return "ORGANIZATION";
    }

    public static function processedDataTypesAvailable()
    {
        return [
            OrganizationType::class => self::FIELD_MANDATORY,
        ];
    }
}