<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 09/08/18
 * Time: 11:28
 */
namespace Aboutgoods\JakkuBundle\DataBag;

use Aboutgoods\JakkuBundle\DataBag\Data\SourceClient;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;
use Aboutgoods\JakkuBundle\DataBag\Data\ProcessedData;
use Aboutgoods\JakkuBundle\DataBag\Data\RawData;
use Composer\Semver\VersionParser;
use Symfony\Component\Validator\Constraints as Assert;

abstract class AbstractDataBag
{
    const FIELD_MANDATORY = 1;
    const FIELD_OPTIONAL = 0;
    /**
     * @var bool
     * @Assert\Type("bool")
     */
    protected $debug = false;
    /**
     * @var string
     * @Assert\Type("string")
     * @Assert\NotBlank()
     */
    protected $bagType;
    /**
     * @var string
     * @Assert\Type("string")
     * @Assert\NotBlank()
     */
    protected $from;
    /**
     * @var ?string
     * @Assert\Type("string")
     */
    protected $description;
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    protected $fromVersion;
    /**
     * @Assert\NotBlank()
     * @Assert\Collection(
     *     fields = {
     *         "needsHumanValidation" = {
     *             @Assert\Type("bool")
     *          },
     *         "validationInstructions"= {}
     *     }
     * )
     */
    protected $verification = ["needsHumanValidation" => false, "validationInstructions" => null];
    /**
     * @var RawData[]
     * @Assert\All({
     *      @Assert\Type("object")
     * })
     */
    protected $rawData = [];
    /**
     * @var ProcessedData[]
     * @Assert\All({
     *      @Assert\Type("object")
     * })
     */
    protected $processedData = [];

    /**
     * @var ?string
     */
    protected $environment = null;
    /**
     * @var ?SourceClient
     */
    protected $sourceClient;

    /**
     * @return mixed
     */
    public function getSourceClient(): ?SourceClient
    {
        return $this->sourceClient;
    }

    /**
     * @param mixed $sourceClient
     *
     * @return AbstractDataBag
     */
    public function setSourceClient(?SourceClient $sourceClient)
    {
        $this->sourceClient = $sourceClient;
        return $this;
    }

    public abstract function getBagType();

    public abstract static function processedDataTypesAvailable();

    public function __construct($debug = false)
    {
        $this->debug = $debug;
        $this->bagType = static::getBagType();
    }

    public function enableHumanVerification($instruction = null): self
    {
        $this->verification["needsHumanValidation"] = true;
        $this->verification["validationInstructions"] = $instruction;

        return $this;
    }

    public function disableHumanVerification(): self
    {
        $this->verification["needsHumanValidation"] = false;
        $this->verification["validationInstructions"] = null;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDebug(): bool
    {
        return $this->debug;
    }

    /**
     * @param bool $debug
     */
    public function setDebug(bool $debug): self
    {
        $this->debug = $debug;

        return $this;
    }

    /**
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * @param string $from
     */
    public function setFrom(string $from): self
    {
        $this->from = $from;

        return $this;
    }

    /**
     * @return string
     */
    public function getFromVersion(): string
    {
        return $this->fromVersion;
    }

    /**
     * @param string $fromVersion
     * @return AbstractDataBag
     */
    public function setFromVersion(string $fromVersion): self
    {
        $version = new VersionParser();
        $this->fromVersion = $version->normalize($fromVersion);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getVerification()
    {
        return $this->verification;
    }

    /**
     * @return RawData[]
     */
    public function getRawData(): array
    {
        return $this->rawData;
    }

    /**
     * @param RawData[] $rawData
     */
    public function setRawData(array $rawData): self
    {
        $this->rawData = $rawData;

        return $this;
    }

    /**
     * @return ProcessedData[]
     */
    public function getProcessedData(): array
    {
        return $this->processedData;
    }

    /**
     * @param ProcessedData[] $processedData
     */
    public function setProcessedData(array $processedData): self
    {
        $this->processedData = $processedData;

        return $this;
    }

    public function addRawData(AbstractDataType $dataType)
    {
        $rawData = new RawData($dataType);
        $this->rawData[] = $rawData;

        return $this;
    }

    public function addProcessedData(AbstractDataType $dataType, ?Source $source = null)
    {
        $processedData = new ProcessedData($dataType, $source);
        $this->processedData[] = $processedData;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return ?string
     */
    public function getEnvironment() : ?string
    {
        return $this->environment;
    }

    /**
     * @param ?string $environment
     */
    public function setEnvironment($environment) : self
    {
        $this->environment = $environment;
        return $this;
    }


}