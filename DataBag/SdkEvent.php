<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 09/08/18
 * Time: 16:32
 */
namespace Aboutgoods\JakkuBundle\DataBag;

class SdkEvent extends AbstractDataBag
{
    const SHOPPING_ENDED = "shoppingEnded";
    const BEGIN_CHECKOUT = "beginCheckout";
    const REFUNDED = "refunded";
    const SELECTED_CONTENT = "selectedContent";
    const SHOPPING_CREATED = "shoppingCreated";
    const SHOPPING_DELETED = "shoppingDeleted";
    const ADDED_PAYMENT_INFOS = "addedPaymentInfos";
    const SIGNED_UP = "signedUp";
    const LOGGED_IN ="loggedIn";
    const LAUNCH_SCAN = "launchScan";
    const IGNORE_SCAN = "ignoreScan";
    const UPLOAD_TICKET = "uploadTicket";
    const ADDED_TO_CART = "addedToCart";
    const BATCH_ADDED_TO_CART = "batchAddedToCart";
    const REMOVED_FROM_CART = "removedFromCart";
    const BATCH_REMOVED_FROM_CART = "batchRemovedFromCart";
    const ADDED_TO_LIST = "addedToList";
    const BATCH_ADDED_TO_LIST = "batchAddedToList";
    const REMOVED_FROM_LIST = "removedFromList";
    const BATCH_REMOVED_FROM_LIST = "batchRemovedFromList";
    const VIEWED = "viewed";
    const SHARED = "shared";
    const SEARCHED = "searched";
    const ADDED_TO_WISHLIST = "addedToWishlist";
    const REMOVED_FROM_WISHLIST = "removedFromWishlist";
    const STARRED = "starred";
    const UNSTARRED = "unstarred";
    const PURCHASED = "purchased";
    const EDITED = "edited";
    const TIMES_LAUNCHED = "timesLaunched";
    const DATE_CHANGED = "dateChanged";
    const SHOW_VIEW = "showView";
    const EXIT_VIEW = "exitView";
    const ACTION = "action";
    const CUSTOM = "custom";

    public function getBagType()
    {
        return "SDK_EVENT";
    }

    public static function processedDataTypesAvailable()
    {
        return [];
    }
}