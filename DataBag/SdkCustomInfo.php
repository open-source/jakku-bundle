<?php


namespace Aboutgoods\JakkuBundle\DataBag;


class SdkCustomInfo extends AbstractDataBag
{
    public function getBagType()
    {
        return "SDK_CUSTOM_INFO";
    }

    public static function processedDataTypesAvailable()
    {
        return [];
    }
}
