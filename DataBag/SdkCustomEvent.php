<?php


namespace Aboutgoods\JakkuBundle\DataBag;


class SdkCustomEvent extends AbstractDataBag
{
    public function getBagType()
    {
        return "SDK_CUSTOM_EVENT";
    }

    public static function processedDataTypesAvailable()
    {
        return [];
    }
}