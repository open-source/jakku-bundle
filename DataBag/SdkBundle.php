<?php


namespace Aboutgoods\JakkuBundle\DataBag;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Device\DeviceApplicationType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Device\DeviceType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Device\NetworkType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person\ApplicationType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person\PersonType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person\SdkType;

class SdkBundle extends AbstractDataBag
{
    public function getBagType()
    {
        return "SDK_BUNDLE";
    }

    public static function processedDataTypesAvailable()
    {
        return [
            PersonType::class,
            DeviceApplicationType::class,
            SdkType::class,
            NetworkType::class,
        ];
    }
}