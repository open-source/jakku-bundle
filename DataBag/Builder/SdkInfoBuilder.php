<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 13/05/2019
 * Time: 09:51
 */

namespace Aboutgoods\JakkuBundle\DataBag\Builder;


use Aboutgoods\JakkuBundle\DataBag\AbstractDataBag;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Info;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person\PersonType;
use Aboutgoods\JakkuBundle\DataBag\Data\RawDataType\ObjectRawType;
use Aboutgoods\JakkuBundle\DataBag\SdkInfo;

class SdkInfoBuilder extends AbstractBuilder
{
    protected $person;
    protected $info;
    protected $rawData;

    protected $env = null;

    public function __construct(
        PersonType $person,
        $rawData,
        Info $info
    )
    {
        $this->person = $person;
        $this->rawData = new ObjectRawType($rawData);
        $this->info = $info;
    }

    /**
     * @return mixed
     */
    public function getEnv()
    {
        return $this->env;
    }

    /**
     * @param mixed $env
     */
    public function setEnv($env)
    {
        $this->env = $env;
    }

    /**
     * @return PersonType
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @param PersonType $person
     */
    public function setPerson(PersonType $person)
    {
        $this->person = $person;
    }

    /**
     * @return Info
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param Info $info
     */
    public function setInfo(Info $info)
    {
        $this->info = $info;
    }

    /**
     * @return mixed
     */
    public function getRawData()
    {
        return $this->rawData;
    }

    /**
     * @param mixed $rawData
     */
    public function setRawData($rawData)
    {
        $this->rawData = new ObjectRawType($rawData);
    }

    public function build()
    {
        parent::build();
        return (new SdkInfo())
            ->addRawData($this->rawData)
            ->addProcessedData($this->person)
            ->addProcessedData($this->info)
            ->setFrom($this->dataBagOrigin)
            ->setSourceClient($this->sourceClient)
            ->setFromVersion($this->dataBagOriginVersion)
            ->setDescription($this->dataBagDescription)
            ->setEnvironment($this->env);
    }

}