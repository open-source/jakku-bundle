<?php


namespace Aboutgoods\JakkuBundle\DataBag\Builder;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Ocr\ReceiptOutputDataType;
use Aboutgoods\JakkuBundle\DataBag\Data\RawDataType\ImageRawDataType;
use Aboutgoods\JakkuBundle\DataBag\Data\RawDataType\ObjectRawType;
use Aboutgoods\JakkuBundle\DataBag\Receipt;
use Aboutgoods\JakkuBundle\DataBag\ReceiptOutput;

class ReceiptOutputBuilder extends AbstractBuilder
{
    protected $receiptOutput;
    protected $rawData;

    /**
     * ReceiptOutputBuilder constructor.
     *
     * @param $receiptOutput
     * @param $rawData
     */
    public function __construct(ReceiptOutputDataType $receiptOutput, $rawData)
    {
        $this->receiptOutput = $receiptOutput;
        $this->rawData = new ImageRawDataType($rawData);
    }

    /**
     * @return mixed
     */
    public function getReceiptOutput()
    {
        return $this->receiptOutput;
    }

    /**
     * @param mixed $receiptOutput
     *
     * @return ReceiptOutputBuilder
     */
    public function setReceiptOutput($receiptOutput)
    {
        $this->receiptOutput = $receiptOutput;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRawData()
    {
        return $this->rawData;
    }

    /**
     * @param mixed $rawData
     *
     * @return ReceiptOutputBuilder
     */
    public function setRawData($rawData)
    {
        $this->rawData = new ImageRawDataType($rawData);
        return $this;
    }

    public function build() {
        parent::build();
        return (new ReceiptOutput())
            ->addRawData($this->rawData)
            ->addProcessedData($this->receiptOutput)
            ->setSourceClient($this->sourceClient)
            ->setFrom($this->dataBagOrigin)
            ->setFromVersion($this->dataBagOriginVersion)
            ->setDescription($this->dataBagDescription);
    }
}