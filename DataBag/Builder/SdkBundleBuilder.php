<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 10/05/2019
 * Time: 09:27
 */

namespace Aboutgoods\JakkuBundle\DataBag\Builder;

use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person\PersonType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Device\DeviceApplicationType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person\SdkType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Device\NetworkType;
use Aboutgoods\JakkuBundle\DataBag\Data\RawDataType\ObjectRawType;
use Aboutgoods\JakkuBundle\DataBag\SdkBundle;

class SdkBundleBuilder extends AbstractBuilder
{
    protected $person;
    protected $rawData;

    protected $deviceApplication = null;
    protected $sdk = null;
    protected $network = null;
    protected $env = null;

    public function __construct(
        PersonType $person,
        $rawData
    )
    {
        $this->person = $person;
        $this->rawData = new ObjectRawType($rawData);
    }

    /**
     * @return mixed
     */
    public function getEnv()
    {
        return $this->env;
    }

    /**
     * @param mixed $env
     */
    public function setEnv($env)
    {
        $this->env = $env;
    }

    /**
     * @param PersonType $person
     */
    public function setPerson(PersonType $person)
    {
        $this->person = $person;
    }

    /**
     * @return PersonType
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @param mixed $rawData
     */
    public function setRawData($rawData)
    {
        $this->rawData = new ObjectRawType($rawData);
    }

    /**
     * @return mixed
     */
    public function getRawData()
    {
        return $this->rawData;
    }

    /**
     * @param DeviceApplicationType|null $deviceApplication
     */
    public function setDeviceApplication(?DeviceApplicationType $deviceApplication)
    {
        $this->deviceApplication = $deviceApplication;
    }

    /**
     * @return DeviceApplicationType|null
     */
    public function getDeviceApplication()
    {
        return $this->deviceApplication;
    }

    /**
     * @param SdkType|null $sdk
     */
    public function setSdk(?SdkType $sdk)
    {
        $this->sdk = $sdk;
    }

    /**
     * @return SdkType|null
     */
    public function getSdk()
    {
        return $this->sdk;
    }


    /**
     * @param networkType|null $network
     */
    public function setNetwork(?NetworkType $network)
    {
        $this->network = $network;
    }

    /**
     * @return NetworkType|null
     */
    public function getNetwork()
    {
        return $this->network;
    }

    public function build() {
        parent::build();
        $dataBag =  (new SdkBundle())
            ->addRawData($this->rawData)
            ->addProcessedData($this->person)
            ->setSourceClient($this->sourceClient)
            ->setEnvironment($this->env)
            ->setFrom($this->dataBagOrigin)
            ->setFromVersion($this->dataBagOriginVersion)
            ->setDescription($this->dataBagDescription);

        if($this->deviceApplication != null) {
            $dataBag->addProcessedData($this->deviceApplication);
        }
        if($this->sdk != null) {
            $dataBag->addProcessedData($this->sdk);
        }
        if($this->network != null) {
            $dataBag->addProcessedData($this->network);
        }

        return $dataBag;
    }
}