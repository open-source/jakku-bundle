<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 14/05/2019
 * Time: 14:52
 */

namespace Aboutgoods\JakkuBundle\DataBag\Builder;


use Aboutgoods\JakkuBundle\DataBag\Data\SourceClient;
use Aboutgoods\JakkuBundle\Exceptions\SourceClientRequiredException;

abstract class AbstractBuilder
{
    protected $dataBagOrigin = null;
    protected $dataBagOriginVersion = null;
    protected $dataBagDescription = null;
    protected $humanVerificationInstruction  = null;
    protected $sourceClient = null;

    /**
     * @return null
     */
    public function getSourceClient():?SourceClient
    {
        return $this->sourceClient;
    }

    /**
     * @param null $sourceClient
     *
     * @return AbstractBuilder
     */
    public function setSourceClient(?SourceClient $sourceClient)
    {
        $this->sourceClient = $sourceClient;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDataBagOrigin()
    {
        return $this->dataBagOrigin;
    }

    /**
     * @param string|null $dataBagOrigin
     *
     * @return AbstractBuilder
     */
    public function setDataBagOrigin(?string $dataBagOrigin)
    {
        $this->dataBagOrigin = $dataBagOrigin;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDataBagDescription()
    {
        return $this->dataBagDescription;
    }

    /**
     * @param string|null $dataBagDescription
     *
     * @return AbstractBuilder
     */
    public function setDataBagDescription(?string $dataBagDescription)
    {
        $this->dataBagDescription = $dataBagDescription;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDataBagOriginVersion()
    {
        return $this->dataBagOriginVersion;
    }

    /**
     * @param string|null $dataBagOriginVersion
     *
     * @return AbstractBuilder
     */
    public function setDataBagOriginVersion(?string $dataBagOriginVersion)
    {
        $this->dataBagOriginVersion = $dataBagOriginVersion;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getHumanVerificationInstruction()
    {
        return $this->humanVerificationInstruction;
    }

    /**
     * @param string|null $humanVerificationInstruction
     *
     * @return AbstractBuilder
     */
    public function setHumanVerificationInstruction($humanVerificationInstruction)
    {
        $this->humanVerificationInstruction = $humanVerificationInstruction;
        return $this;
    }

    protected function build() {
        if($this->sourceClient == null || $this->sourceClient->getClientIdentifier() == null) {
            throw new SourceClientRequiredException();
        }
    }

}