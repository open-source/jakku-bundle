<?php


namespace Aboutgoods\JakkuBundle\DataBag\Builder;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person\ApplicationType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person\PersonType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\TradeAction\ReceiptType;
use Aboutgoods\JakkuBundle\DataBag\Data\RawDataType\ObjectRawType;
use Aboutgoods\JakkuBundle\DataBag\PersonReceipt;

class PersonReceiptBuilder extends AbstractBuilder
{
    protected $person;
    protected $receipt;
    protected $rawData;
    protected $application;

    /**
     * @return mixed
     */
    public function getApplication():?ApplicationType
    {
        return $this->application;
    }

    /**
     * @param ApplicationType $application
     *
     * @return PersonReceiptBuilder
     */
    public function setApplication(?ApplicationType $application)
    {
        $this->application = $application;
        return $this;
    }

    /**
     * PersonReceiptBuilder constructor.
     *
     * @param PersonType $person
     * @param ReceiptType $receipt
     * @param $rawData
     */
    public function __construct(
        PersonType $person,
        ReceiptType $receipt,
        $rawData
    )
    {
        $this->person = $person;
        $this->receipt = $receipt;
        $this->rawData = new ObjectRawType($rawData);
    }

    /**
     * @return mixed
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @param mixed $person
     *
     * @return PersonReceiptBuilder
     */
    public function setPerson($person)
    {
        $this->person = $person;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReceipt()
    {
        return $this->receipt;
    }

    /**
     * @param mixed $receipt
     *
     * @return PersonReceiptBuilder
     */
    public function setReceipt($receipt)
    {
        $this->receipt = $receipt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRawData()
    {
        return $this->rawData;
    }

    /**
     * @param mixed $rawData
     */
    public function setRawData($rawData)
    {
        $this->rawData = new ObjectRawType($rawData);
    }

    public function build() {
        parent::build();
        $databag = (new PersonReceipt())
            ->addRawData($this->rawData)
            ->setFrom($this->dataBagOrigin)
            ->setFromVersion($this->dataBagOriginVersion)
            ->setDescription($this->dataBagDescription)
            ->setSourceClient($this->sourceClient)
            ->addProcessedData($this->receipt)
            ->addProcessedData($this->person)
            ->setDescription($this->dataBagDescription);
        if ($this->application !== null){
            $databag->addProcessedData($this->application);
        }
        return $databag;
    }
}