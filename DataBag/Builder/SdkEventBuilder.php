<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 10/05/2019
 * Time: 15:32
 */

namespace Aboutgoods\JakkuBundle\DataBag\Builder;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\AbstractDataType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person\ApplicationActionType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person\PersonType;
use Aboutgoods\JakkuBundle\DataBag\Data\RawDataType\ObjectRawType;
use Aboutgoods\JakkuBundle\DataBag\SdkEvent;

class SdkEventBuilder extends AbstractBuilder
{
    protected $person;
    protected $applicationAction;
    protected $rawData;

    protected $env = null;

    public function __construct(
        PersonType $person,
        ApplicationActionType $applicationAction,
        $rawData
)
    {
        $this->person = $person;
        $this->applicationAction = $applicationAction;
        $this->rawData = new ObjectRawType($rawData);
    }

    /**
     * @return mixed
     */
    public function getEnv()
    {
        return $this->env;
    }

    /**
     * @param mixed $env
     */
    public function setEnv($env)
    {
        $this->env = $env;
    }

    /**
     * @return PersonType
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @param PersonType $person
     */
    public function setPerson(PersonType $person)
    {
        $this->person = $person;
    }

    /**
     * @return ApplicationActionType
     */
    public function getApplicationAction()
    {
        return $this->applicationAction;
    }

    /**
     * @param ApplicationActionType $applicationAction
     */
    public function setApplicationAction(ApplicationActionType $applicationAction)
    {
        $this->applicationAction = $applicationAction;
    }

    /**
     * @return mixed
     */
    public function getRawData()
    {
        return $this->rawData;
    }

    /**
     * @param mixed $rawData
     */
    public function setRawData( $rawData)
    {
        $this->rawData = new ObjectRawType($rawData);
    }

    public function build() {
        parent::build();
        return (new SdkEvent())
            ->addRawData($this->rawData)
            ->setEnvironment($this->env)
            ->setSourceClient($this->sourceClient)
            ->setFrom($this->dataBagOrigin)
            ->setFromVersion($this->dataBagOriginVersion)
            ->setDescription($this->dataBagDescription)
            ->addProcessedData($this->person)
            ->addProcessedData($this->applicationAction);
    }
}