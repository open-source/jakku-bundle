<?php


namespace Aboutgoods\JakkuBundle\DataBag\Builder;


use Aboutgoods\JakkuBundle\DataBag\AbstractDataBag;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\TradeAction\TradeActionType;
use Aboutgoods\JakkuBundle\DataBag\Data\RawDataType\ImageRawDataType;
use Aboutgoods\JakkuBundle\DataBag\Receipt;

class ReceiptBuilder extends AbstractBuilder
{
    protected $tradeAction;
    protected $productTradeActionList = [];
    protected $rawData;

    protected $application = null;

    public function __construct(
        TradeActionType $tradeAction,
        $rawData
    )
    {
        $this->tradeAction = $tradeAction;
        $this->rawData = new ImageRawDataType($rawData);
    }

    /**
     * @return mixed
     */
    public function getTradeAction()
    {
        return $this->tradeAction;
    }

    /**
     * @param mixed $tradeAction
     *
     * @return ReceiptBuilder
     */
    public function setTradeAction($tradeAction)
    {
        $this->tradeAction = $tradeAction;
        return $this;
    }

    /**
     * @return array
     */
    public function getProductTradeActionList()
    {
        return $this->productTradeActionList;
    }

    /**
     * @param array $productTradeActionList
     *
     * @return ReceiptBuilder
     */
    public function setProductTradeActionList(array $productTradeActionList): ReceiptBuilder
    {
        $this->productTradeActionList = $productTradeActionList;
        return $this;
    }

    /**
     * @return null
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * @param null $application
     *
     * @return ReceiptBuilder
     */
    public function setApplication($application)
    {
        $this->application = $application;
        return $this;
    }

    /**
     * @return ImageRawDataType
     */
    public function getRawData(): ImageRawDataType
    {
        return $this->rawData;
    }

    /**
     * @param ImageRawDataType $rawData
     */
    public function setRawData(ImageRawDataType $rawData): void
    {
        $this->rawData = new ImageRawDataType($rawData);
    }

    public function build() {

        $databag = (new Receipt())
            ->addRawData($this->rawData)
            ->addProcessedData($this->tradeAction)
            ->setSourceClient($this->sourceClient)
            ->setFrom($this->dataBagOrigin)
            ->setFromVersion($this->dataBagOriginVersion)
            ->setDescription($this->dataBagDescription);
        if ($this->application !== null){
            $databag->addProcessedData($this->application);
        }
        foreach ($this->productTradeActionList as $productTradeAction){
            $databag->addProcessedData($productTradeAction);
        }
        return $databag;
    }
}