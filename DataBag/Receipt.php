<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 09/08/18
 * Time: 16:32
 */
namespace Aboutgoods\JakkuBundle\DataBag;

use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person\PersonType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\PointOfSale\PointOfSaleType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Product\ProductType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Source\ImageType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\TradeAction\TradeActionType;

class Receipt extends AbstractDataBag
{
    public function getBagType()
    {
        return "RECEIPT";
    }

    public static function processedDataTypesAvailable()
    {
        return [
            ImageType::class => self::FIELD_OPTIONAL,
            PointOfSaleType::class  => self::FIELD_OPTIONAL,
            ProductType::class => self::FIELD_OPTIONAL,
            TradeActionType::class => self::FIELD_OPTIONAL,
            PersonType::class => self::FIELD_OPTIONAL
        ];
    }
}