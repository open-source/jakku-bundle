<?php


namespace Aboutgoods\JakkuBundle\DataBag;


use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Ocr\ReceiptOutputDataType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person\PersonType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\PointOfSale\PointOfSaleType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Product\ProductType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Source\ImageType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\TradeAction\TradeActionType;

class ReceiptOutput extends AbstractDataBag
{
    public function getBagType()
    {
        return "RECEIPT_OUTPUT";
    }

    public static function processedDataTypesAvailable()
    {
        return [
            ReceiptOutputDataType::class => self::FIELD_MANDATORY
        ];
    }
}