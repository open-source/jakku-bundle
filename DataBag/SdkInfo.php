<?php
/**
 * Created by PhpStorm.
 * User: aboutgoods
 * Date: 20/03/19
 * Time: 13:32
 */

namespace Aboutgoods\JakkuBundle\DataBag;

use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Device\DeviceApplicationType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Device\NetworkType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person\PersonType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person\SdkType;

class SdkInfo extends AbstractDataBag
{
    const USERNAME = "username";
    const USER_BIRTHDAY = "userBirthday";
    const USER_GENDER = "userGender";
    const USER_NATIONALITY = "userNationality";
    const USER_FIRSTNAME = "userFirstName";
    const USER_LASTNAME = "userLastName";
    const USER_MAIL = "userMail";
    const USER_PICTURE_URL = "userPictureUrl";
    const USER_HOUSEHOLD_SIZE = "userHouseholdSize";
    const WIFI_BSSID = "wifiBssid";
    const WIFI_SSID = "wifiSsid";
    const GPS_COORDINATE = "gpsCoordinate";
    const IP_MOBILE = "ipMobile";
    const IP_WIFI = "ipWifi";
    const CUSTOM = "custom";

    public function getBagType()
    {
        return "SDK_INFO";
    }

    public static function processedDataTypesAvailable()
    {
        return [
            PersonType::class,
            DeviceApplicationType::class,
            SdkType::class,
            NetworkType::class,
        ];
    }
}