<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 16/11/18
 * Time: 14:29
 */

namespace Aboutgoods\JakkuBundle\DataBag;


class Person extends AbstractDataBag
{

    public function getBagType()
    {
        return "PERSON";
    }

    public static function processedDataTypesAvailable()
    {
        return [];
    }
}