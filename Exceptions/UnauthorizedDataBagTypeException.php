<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 7/16/18
 * Time: 2:32 PM
 */
namespace Aboutgoods\JakkuBundle\Exceptions;

use Throwable;

class UnauthorizedDataBagTypeException extends \Exception
{
    public function __construct($expected = [], $given)
    {
        parent::__construct("Unknown $given databag, expected ".implode(",", $expected));
    }
}