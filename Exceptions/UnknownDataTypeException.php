<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 7/13/18
 * Time: 10:08 AM
 */
namespace Aboutgoods\JakkuBundle\Exceptions;

use Throwable;

class UnknownDataTypeException extends \Exception
{
    public function __construct($datatype = "")
    {
        parent::__construct("Unknown data type ".$datatype);
    }
}