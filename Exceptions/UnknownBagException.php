<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 7/16/18
 * Time: 3:01 PM
 */
namespace Aboutgoods\JakkuBundle\Exceptions;

class UnknownBagException extends \Exception
{
}