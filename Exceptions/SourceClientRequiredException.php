<?php


namespace Aboutgoods\JakkuBundle\Exceptions;


class SourceClientRequiredException extends \Exception
{
    public function __construct()
{
    parent::__construct("SourceClient's clientIdentifier is required for every databag");
}
}