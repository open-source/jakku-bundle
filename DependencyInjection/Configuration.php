<?php
namespace Aboutgoods\JakkuBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('jakku');
        $defaultConnectValue = [
            "address"  => "rabbitmq://guest:guest@localhost:5672/jakku",
        ];
        $rootNode
            ->children()
                ->arrayNode("rabbitmq")
                    ->children()
                        ->scalarNode('address')->defaultValue("rabbitmq://guest:guest@localhost:5672/jakku")->end()
                    ->end()
                ->end()//rabbitmq
                ->arrayNode("application")
                    ->children()
                        ->scalarNode("name")->defaultValue("my awesome app")->end()
                        ->scalarNode("version")->defaultValue("0.0.1-alpha")->end()
                        ->arrayNode('bags')->isRequired()
                            ->scalarPrototype()->end()
                        ->end()
                    ->end()
                ->end()//application
            ->end()
        ;
        return $treeBuilder;
    }
}
