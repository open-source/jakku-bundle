Jakku Bundle
====================
add to composer.json
```json
{
    //...
    "require": {
      //...
      "aboutgoods/jakku-bundle":"^0.4"
      //...
    }
}
```
If you use the jakku-bundle, you must declare the dependency of php-jakku-client manually.

then run `composer install`.

**SF <4**   
You need to add             
```
new \Aboutgoods\JakkuBundle\JakkuBundle(),
```
in your appKernel file.

**SF 4+**  
You need to add             
```
    \Aboutgoods\JakkuBundle\JakkuBundle::class            => ['all' => true],
```
in /config/bundles.php


## Configuration

```yaml
# Default configuration for "JakkuBundle"
jakku:
    rabbitmq:             # Required - RabbitMQ information for Jakku Client
        address:              rabbitmq://guest:guest/127.0.0.1:5672/jakku
    application:          # Required 
        name:                 'my awesome app'
        version:              0.0.0.1-alpha
        bags:                 [ "TRANSACTION", "RECEIPT","ACTION","DEBUG"] #allowed bags for developers - default none
        
```

## Test

If you want to test databag builders directly on this project without use it on another project, you must follow these steps :

- Run `composer install`
- Uncomment the wanted function at the end of `Tests/TestBuilder.php`
- Run `php Tests/TestBuilder.php`

> A JSON must appear on your console 🎉