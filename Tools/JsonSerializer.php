<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 6/29/18
 * Time: 2:14 PM
 */
namespace Aboutgoods\JakkuBundle\Tools;

use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\JsonSerializableNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class JsonSerializer
{
    private static $serializer = null;

    public static function getInstance()
    {
        if (self::$serializer == null) {
            $encoders = [new JsonEncoder()];
            $normalizers = [
                new DateTimeNormalizer(\DateTime::ISO8601),
                new JsonSerializableNormalizer(),
                new ObjectNormalizer(),
            ];
            self::$serializer = new Serializer($normalizers, $encoders);
        }

        return self::$serializer;
    }

    /**
     * @param $object
     *
     * @return bool|float|int|string
     */
    public static function stringify($object)
    {
        return self::getInstance()->serialize($object, "json");
    }
}