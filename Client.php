<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 7/16/18
 * Time: 9:48 AM
 */
namespace Aboutgoods\JakkuBundle;

use Aboutgoods\JakkuBundle\Connector;
use Aboutgoods\JakkuBundle\DataBag\AbstractDataBag;
use Aboutgoods\JakkuBundle\Exceptions\UnknownBagException;
use Aboutgoods\JakkuBundle\Exceptions\UnauthorizedDataBagTypeException;

class Client
{
    protected $configuration;

    public function getConfiguration()
    {
        return $this->configuration;
    }

    public function setConfiguration($config)
    {
        // Default configuration
        $this->configuration = [
            "rabbitmq"    => [
                "address"  => "rabbitmq://guest:guest@localhost:5672/jakku"
            ],
            "application" => [
                "bags"    => [],
                "version" => "0.0.1",
                "name"    => "jakku bundle",
            ],
        ];

        // Replace default values by values of jakku.yaml
        foreach ($this->configuration as $k => $v) {
            foreach ($v as $subk => $subv) {
                if (isset($config[$k][$subk])) {
                    $this->configuration[$k][$subk] = $config[$k][$subk];
                }
            }
        }
    }

    /**
     * @param AbstractDataBag $dataBag
     *
     * @return bool
     * @throws UnauthorizedDataBagTypeException
     * @throws \Aboutgoods\JakkuBundle\Exceptions\InvalidObjectException
     */
    public function send(AbstractDataBag $dataBag)
    {
        Connector::connect($this->configuration["rabbitmq"]);
        $dataBag->setFromVersion($this->configuration["application"]["version"]);
        $dataBag->setFrom($this->configuration["application"]["name"]);
        if ( ! in_array($dataBag->getBagType(), $this->configuration["application"]["bags"])) {
            throw new UnauthorizedDataBagTypeException($this->configuration["application"]["bags"],
                $dataBag->getBagType());
        }
        Connector::emit($dataBag);

        return true;
    }
}