<?php

namespace Aboutgoods\JakkuBundle\Tests;

use Aboutgoods\JakkuBundle\DataBag\Builder\PersonReceiptBuilder;
use Aboutgoods\JakkuBundle\DataBag\Builder\ReceiptBuilder;
use Aboutgoods\JakkuBundle\DataBag\Builder\ReceiptOutputBuilder;
use Aboutgoods\JakkuBundle\DataBag\Builder\SdkBundleBuilder;
use Aboutgoods\JakkuBundle\DataBag\Builder\SdkEventBuilder;
use Aboutgoods\JakkuBundle\DataBag\Builder\SdkInfoBuilder;
use Aboutgoods\JakkuBundle\DataBag\Data\SourceClient;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Device\DeviceApplicationType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Device\DeviceType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Device\NetworkType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Device\WifiType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Ocr\ReceiptOutputDataType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Info;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Parameter;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person\ActionType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person\ApplicationActionType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person\ApplicationType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person\PersonType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\Person\SdkType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\TradeAction\ProductTradeActionType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\TradeAction\ReceiptType;
use Aboutgoods\JakkuBundle\DataBag\Data\DataType\TradeAction\TradeActionType;
use Aboutgoods\JakkuBundle\DataBag\Data\RawDataType\ImageRawDataType;
use Aboutgoods\JakkuBundle\DataBag\ReceiptOutput;
use Aboutgoods\JakkuBundle\DataBag\SdkEvent;
use Aboutgoods\JakkuBundle\DataBag\SdkInfo;
use Aboutgoods\JakkuBundle\JakkuBundle;
use Aboutgoods\JakkuBundle\Tools\JsonSerializer;

require __DIR__ . '/../vendor/autoload.php';

function sdkBundleBuilderSample() {
    $dataBagOrigin = "Jakku-Bundle Tester";

    //CONSTRUCTION OF PERSON
    $person = (new PersonType())
        ->setUuid("d0c0fa42-689e-4bbc-a1a0-b07d4ee54e21");

    //CONSTRUCTION OF DEVICE_APPLICATION
    $device = (new DeviceType())
        ->setUdid("00008020-008D4548007B4F26")
        ->setModel("iPhone 11")
        ->setManufacturer("Apple")
        ->setLocale("FR")
        ->setOs("iOS")
        ->setOsVersion("13.1.2")
        ->setBatteryLevel("80%")
        ->setCurrencyCode("€");
    $application = (new ApplicationType())
        ->setName("TACHETKOA")
        ->setVersionBuild("30")
        ->setVersionName("3.2");
    $deviceApplication = (new DeviceApplicationType())
        ->setDevice($device)
        ->setApplication($application)
        ->setTimesLaunched(60)
        ->setInstanceId("cJZ0aAAw3fo");

    //CONSTRUCTION OF SDK
    $sdk = (new SdkType())
        ->setVersionBuild("32")
        ->setVersionName("2.3");

    //CONSTRUCTION OF NETWORK
    $network = (new NetworkType())
        ->setWifi(
            (new WifiType())
            ->setSSID("29-29-03-AB-B3")
            );


    $sdkBundleBuilder = new SdkBundleBuilder(
        $person, $application
    );

    $sdkBundleBuilder->setDeviceApplication($deviceApplication);
    $sdkBundleBuilder->setSdk($sdk);
    $sdkBundleBuilder->setNetwork($network);
    $sdkBundleBuilder->setDataBagOrigin($dataBagOrigin);
    $sdkBundleBuilder->setDataBagOriginVersion("0.4.0");
    $sdkBundleBuilder->setDataBagDescription("Test SDKBundle Builder");
    $sdkBundleBuilder->setSourceClient((new SourceClient())->setClientIdentifier("dev-company-uid"));

    $dataBag = $sdkBundleBuilder->build();
    print(JsonSerializer::stringify($dataBag));
}

function sdkEventBuilderSample() {
    $dataBagOrigin = "Jakku-Bundle Tester";

    //CONSTRUCTION OF PERSON
    $person = (new PersonType())
        ->setUuid("d0c0fa42-689e-4bbc-a1a0-b07d4ee54e21");

    //CONSTRUCTION OF APPLICATION_ACTION

    $application = (new ApplicationType())
        ->setName("TACHETKOA")
        ->setVersionBuild("30")
        ->setVersionName("3.2");

    $key = SdkEvent::ADDED_TO_CART;

    $firstParameter = new Parameter();
    $firstParameter->setKey("itemName");
    $firstParameter->setData("Tomate");

    $secondParameter = new Parameter();
    $secondParameter->setKey("price");
    $secondParameter->setData("2.9");

    $parametersList = array($firstParameter, $secondParameter);

    $action = (new ActionType())
        ->setKey($key)
        ->setValue($parametersList);

    $applicationAction = (new ApplicationActionType())
        ->setApplication($application)
        ->setAction($action)
        ->setDate(date("m.d.y"));

    $sdkEventBuilder = new SdkEventBuilder(
        $person,
        $applicationAction,
        $person
    );

    $sdkEventBuilder->setDataBagOrigin($dataBagOrigin);
    $sdkEventBuilder->setDataBagOriginVersion("0.4.0");
    $sdkEventBuilder->setDataBagDescription("Test SDKEvent Builder");
    $sdkEventBuilder->setSourceClient((new SourceClient())->setClientIdentifier("dev-company-uid"));

    $dataBag = $sdkEventBuilder->build();
    print(JsonSerializer::stringify($dataBag));
}

function sdkInfoBuilderSample() {
    $dataBagOrigin = "Jakku-Bundle Tester";

    $person = (new PersonType())
        ->setUuid("263e2b4c-c618-4a9f-acc1-1d3bbd6b27c2");


    $key = SdkInfo::USER_GENDER;

    $firstParameter = new Parameter();
    $firstParameter->setData("male");
    $firstParameter->setKey("sexe");

    $secondParameter = new Parameter();
    $secondParameter->setKey("sexe");
    $secondParameter->setData("female");

    $parametersList = array($firstParameter, $secondParameter);

    $info = (new Info())
        ->setKey($key)
        ->setValue($parametersList);


    $sdkInfoBuilder = new SdkInfoBuilder(
        $person,
        $person,
        $info
    );

    $sdkInfoBuilder->setDataBagOrigin($dataBagOrigin);
    $sdkInfoBuilder->setDataBagOriginVersion("0.4.0");
    $sdkInfoBuilder->setDataBagDescription("Test SDKInfo Builder");
    $sdkInfoBuilder->setSourceClient((new SourceClient())->setClientIdentifier("dev-company-uid"));

    $databag = $sdkInfoBuilder->build();
    print(JsonSerializer::stringify($databag));
}

function receiptBuilderSample() {
    $dataBagOrigin = "Jakku-Bundle Tester";

    $tradeAction = (new TradeActionType())
                ->setUid('dde41f30-9dab-4556-8f2b-897535cc53d4');

    $productTradeAction = [(new ProductTradeActionType())
                ->setQuantity(2),(new ProductTradeActionType())
        ->setQuantity(6)];

    //CONSTRUCTION OF APPLICATION_ACTION

    $application = (new ApplicationType())
        ->setName("TACHETKOA")
        ->setVersionBuild("30")
        ->setVersionName("3.2");

    $receiptBuilder = new ReceiptBuilder(
        $tradeAction,
        $dataBagOrigin
    );
    $receiptBuilder->setApplication($application);
    $receiptBuilder->setProductTradeActionList($productTradeAction);
    $receiptBuilder->setDataBagOrigin($dataBagOrigin);
    $receiptBuilder->setSourceClient((new SourceClient())->setClientIdentifier("dev-company-uid"));
    $receiptBuilder->setDataBagOriginVersion("0.4.0");
    $receiptBuilder->setDataBagDescription("Test Receipt Builder");

    $dataBag = $receiptBuilder->build();
    print(JsonSerializer::stringify($dataBag));
}

function receiptOutputBuilderSample() {
    $dataBagOrigin = "Jakku-Bundle Tester";

    $rawData = (new ImageRawDataType('https://google.com'));
    $receiptOutput = (new ReceiptOutputDataType())->setUid('fb0c6162-a019-48d1-a7e1-1f17ef10f97b')->setName("thats a receipt")->setData('{}')->setVersion('2.0');

    $receiptOutputBuilder = new ReceiptOutputBuilder(
        $receiptOutput,
        $rawData
    );

    $receiptOutputBuilder->setDataBagOrigin($dataBagOrigin);
    $receiptOutputBuilder->setDataBagOriginVersion("0.4.0");
    $receiptOutputBuilder->setDataBagDescription("Test ReceiptOutput Builder");
    $receiptOutputBuilder->setSourceClient((new SourceClient())->setClientIdentifier("dev-company-uid"));

    $dataBag = $receiptOutputBuilder->build();
    print(JsonSerializer::stringify($dataBag));
}

function personReceiptBuilderSample() {
    $dataBagOrigin = "Jakku-Bundle Tester";

    $person = (new PersonType())->setFirstname('Bob')->setUuid('e5576948-a066-4311-8e4e-a45be2e4b311');
    $receipt = (new ReceiptType())->setName('Franck');

    $personReceiptBuilder = new PersonReceiptBuilder(
            $person,
            $receipt,
            $dataBagOrigin
    );

    $personReceiptBuilder->setDataBagOriginVersion("0.4.0");
    $personReceiptBuilder->setDataBagOrigin($dataBagOrigin);
    $personReceiptBuilder->setDataBagDescription("Test PersonReceipt Buider");
    $personReceiptBuilder->setSourceClient((new SourceClient())->setClientIdentifier("dev-company-uid"));

    $dataBag = $personReceiptBuilder->build();
    print(JsonSerializer::stringify($dataBag));
}

/* ------- TEST SECTION ------- */

sdkBundleBuilderSample();
//sdkEventBuilderSample();
//sdkInfoBuilderSample();
//receiptBuilderSample();
//receiptOutputBuilderSample();
//personReceiptBuilderSample();
