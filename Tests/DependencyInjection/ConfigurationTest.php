<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 7/30/18
 * Time: 10:54 AM
 */
namespace Aboutgoods\JakkuBundle\Tests\DependencyInjection;

use Aboutgoods\JakkuBundle\DataBag\Debug;
use Aboutgoods\JakkuBundle\DataBag\Transaction;
use Aboutgoods\JakkuBundle\DependencyInjection\Configuration;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Config\Definition\Processor;

class ConfigurationTest extends TestCase
{
    public function testValidConfiguration()
    {
        $config = [
            "rabbitmq"    => [
                "address"  => "localhost",
                "port"     => 5672,
                "login"    => "guest",
                "password" => "guest",
                "queue"    => "jakku",
            ],
            "application" => [
                "bags"    => [Transaction::getBagType(), Debug::getBagType()],
                "version" => "0.0.1",
                "name"    => "jakku bundle",
            ],
        ];
        $configs = [$config];
        $processor = new Processor();
        $databaseConfiguration = new Configuration();
        $processedConfiguration = $processor->processConfiguration(
            $databaseConfiguration,
            $configs
        );
        $this->assertEquals($config, $processedConfiguration);
    }


}