<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 7/30/18
 * Time: 9:31 AM
 */
namespace Aboutgoods\JakkuBundle\Tests;

use Aboutgoods\JakkuBundle\DataBag\DataBagFactory;
use Aboutgoods\JakkuBundle\DataBag\Debug;
use Aboutgoods\JakkuBundle\DataBag\Receipt;
use Aboutgoods\JakkuBundle\DataBag\Transaction;
use Aboutgoods\JakkuBundle\DataTypes\DataTypeNormalizer;
use Aboutgoods\JakkuBundle\DataTypes\RawData;
use Aboutgoods\JakkuBundle\Exceptions\InvalidObjectException;
use Aboutgoods\JakkuBundle\Exceptions\UnknownBagException;
use Aboutgoods\JakkuBundle\Client;
use Aboutgoods\JakkuBundle\Exceptions\UnauthorizedDataBagTypeException;
use PHPUnit\Framework\TestCase;

/**
 * Class ClientTest
 *
 * @package Aboutgoods\JakkuBundle\Tests
 * @codeCoverageIgnore
 */
class ClientTest extends TestCase
{
    public function testClient()
    {
        $client = new Client();
        $client->setConfiguration([
            "rabbitmq"    => [
                "address"  => "localhost",
                "port"     => 5672,
                "login"    => "guest",
                "password" => "guest",
                "queue"    => "jakku",
            ],
            "application" => [
                "bags"    => [Transaction::getBagType(), Debug::getBagType()],
                "version" => "0.0.1",
                "name"    => "jakku bundle",
            ],
        ]);
        $dataBag = DataBagFactory::create(Debug::getBagType());
        $dataBag->setReceivedOn(new \DateTime());
        $dataBag->addData(RawData::create(RawData::BRAND, ["ok" => "go"]));
        $this->assertTrue($client->send($dataBag));

    }

    public function testClientConfigurationError()
    {
        $this->expectException(UnknownBagException::class);
        $client = new Client();
        $client->setConfiguration([
            "rabbitmq"    => [
                "address"  => "localhost",
                "port"     => 5672,
                "login"    => "guest",
                "password" => "guest",
                "queue"    => "jakku",
            ],
            "application" => [
                "bags"    => ["pasta"],
                "version" => "0.0.1",
                "name"    => "jakku bundle",
            ],
        ]);
    }

    public function testClientNotAllowedBag()
    {
        $this->expectException(UnauthorizedDataBagTypeException::class);
        $client = new Client();
        $client->setConfiguration([
            "rabbitmq"    => [
                "address"  => "localhost",
                "port"     => 5672,
                "login"    => "guest",
                "password" => "guest",
                "queue"    => "jakku",
            ],
            "application" => [
                "bags"    => [Transaction::getBagType(), Debug::getBagType()],
                "version" => "0.0.1",
                "name"    => "jakku bundle",
            ],
        ]);
        $dataBag = DataBagFactory::create(Receipt::getBagType());
        $this->assertInstanceOf(Receipt::class, $dataBag);
        if ($dataBag instanceof Receipt) {
            $dataBag->setReceivedOn(new \DateTime());
        }
        $dataBag->addData(RawData::create(RawData::BRAND, ["ok" => "go"]));
        $client->send($dataBag);
    }
}