<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 7/30/18
 * Time: 11:55 AM
 */
namespace Aboutgoods\JakkuBundle\Tests;

use Aboutgoods\JakkuBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ServiceTest extends WebTestCase
{
    public function testClient()
    {
        $client = static::createClient();
        $service = $client->getContainer()->get("jakku.client");
        $configuration = $service->getConfiguration();
        $this->assertInstanceOf(Client::class, $service);
        $this->assertArrayHasKey("rabbitmq", $configuration);
        $this->assertArrayHasKey("application", $configuration);
    }
}